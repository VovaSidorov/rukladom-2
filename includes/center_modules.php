<?php
/* -----------------------------------------------------------------------------------------
   $Id: center_modules.php 899 2007-02-06 20:14:56 VaM $   

   VaM Shop - open source ecommerce solution
   http://vamshop.ru
   http://vamshop.com

   Copyright (c) 2007 VaM Shop
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommercebased on original files from OSCommerce CVS 2.2 2002/08/28 02:14:35 www.oscommerce.com 
   (c) 2003	 nextcommerce (center_modules.php,v 1.5 2003/08/13); www.nextcommerce.org 
   (c) 2004	 xt:Commerce (center_modules.php,v 1.5 2003/08/13); xt-commerce.com 

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

  require(DIR_WS_MODULES . FILENAME_NEW_PRODUCTS);

  require(DIR_WS_MODULES . FILENAME_FEATURED);

  require(DIR_WS_MODULES . FILENAME_NEWS);

  require(DIR_WS_MODULES . FILENAME_UPCOMING_PRODUCTS);
  require(DIR_WS_MODULES . 'reviews_main.php');
  
/*plugins | 160.by @16*/
  $m = apply_filter('center_modules', $m);
  
  if (is_array($m) and count($m)>0)
  {
     foreach ($m as $n=>$v)
	 {
	     $default->assign($n, $v);
	 }
  }
/*plugins | 160.by @16*/

  return $module;
?>