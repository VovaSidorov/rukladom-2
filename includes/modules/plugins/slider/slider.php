<?php
    /*
    Plugin Name: Баннеры  
    Plugin URI: http://160.by/
    Version: 1.2
    Author: Евгений Фоминов
    Author URI: http://160.by/
    */
	
    add_filter('menu_modules',    'slider::menu');
    add_action('main_page_admin', 'slider::page');
    add_filter('main_assign',     'slider::info');
    add_filter('center_modules',  'slider::info');
    add_filter('product_info',  'slider::info');
    add_filter('product_listing',  'slider::info');

    class slider
	{
	   public static function page()
	   {
	      include('slider.page.php');
	   }
	   
	   public static function remove()
	   {
	      p::query("drop table slider;");
	   }
	   
	   public static function install()
	   {
	        p::query("CREATE TABLE IF NOT EXISTS slider (
            slider_id int NOT NULL auto_increment,
            slider_url varchar(255) NOT NULL,
            slider_img varchar(255) NOT NULL,
            slider_sort int NOT NULL,
            language_id int NOT NULL,
            slider_title varchar(255) NOT NULL,
            slider_text1 varchar(255) NOT NULL,
            slider_text2 varchar(255) NOT NULL,
            slider_group varchar(255) NOT NULL,
            PRIMARY KEY  (slider_id)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;");

        add_option('slider_status', 'true', 'radio', 'array("true", "false")');
	   }
	   
	   public static function menu($arr)
	   {
	      $arr[] =  array(
            'code' => 'slider',
            'title' => 'Баннеры',
            'link' => 'plugins.php?main_page=slider::page'
           ); 
		   
	      return $arr;
	   }   
	   
       public static function info($ar)
       {
	       $group = array();

           $s = p::query('select * from slider where language_id='.$_SESSION['languages_id'].' or language_id=0 order by slider_sort');
           while ($n = p::fetch_array($s))
           {
		      if ( !isset($group [ $n['slider_group'] ] )) $group [ $n['slider_group'] ]  = array();
		      if ( !empty($n['slider_group']))
			  {
			     $group [ $n['slider_group'] ][]  = $n;
			  }
           } 

		   if ( is_array($group) && count($group) > 0)
		   {
		      foreach ($group as $name=>$val)
			  {
			     $ar[ $name ] = $val; 
			  }
		   }
         
           return $ar;		   
       }
	   
	   //osc
	   public static function info2($_group_name)
       {
	       $ar = array();
	       $group = array();
           $s = p::query('select * from slider order by slider_sort');
           while ($n = p::fetch_array($s))
           {
		      if ( !isset($group [ $n['slider_group'] ] )) $group [ $n['slider_group'] ]  = array();
		      if ( !empty($n['slider_group']))
			  {
			     $group [ $n['slider_group'] ][]  = $n;
			  }
           } 

		   if ( is_array($group) && count($group) > 0)
		   {
		      foreach ($group as $name=>$val)
			  {
			     $ar[ $name ] = $val; 
			  }
		   }

		   if ( isset( $ar [ $_group_name ] ))
		   {
		      return $ar [ $_group_name ];
		   }		   
       }
	   //osc
	   public static function run()
	   {

	       include('slider.run.php');
	   }
	}



?>