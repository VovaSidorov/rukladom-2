<style>
    a.reder {color:red}
</style>
<?php
include(DIR_WS_CLASSES . 'language.php');
$lng = new language;


$languages_string = '';
$count_lng='';



reset($lng->catalog_languages); 

$am = array();
foreach ($lng->catalog_languages as $_n => $_v)
{
    $am[ $_v['id'] ] = $_n; 
} 

if ( isset($_GET['act']) && $_GET['act'] == 'update' && isset($_GET['id']))
{
    if ( isset($_FILES['slider_img']) and !empty($_FILES['slider_img']['name']))
    {
        $targetFile = DIR_FS_CATALOG.'images/slider/'. $_FILES['slider_img']['name'];
        $tempFile = $_FILES['slider_img']['tmp_name'];
        move_uploaded_file($tempFile, $targetFile);

        if ( is_file($targetFile))
        {
            $_POST['slider_img'] = $_FILES['slider_img']['name'];

        }
    } 
    else
    {
        if (!empty($_POST['slider_filename']))
        {
            $_POST['slider_img'] = $_POST['slider_filename'];
        }
    }	
    unset( $_POST['slider_filename'] );	
    $_POST['slider_sort'] = (int)$_POST['slider_sort'];		
    p::perform('slider', p::prepare_input($_POST), 'update', 'slider_id="'.(int)$_GET['id'].'"');
}



if ( isset($_GET['act']) and $_GET['act'] == 'add')
{
    $slider_url = p::prepare_input($_POST['slider_url']);
    $slider_title = p::prepare_input($_POST['slider_title']);
    $slider_text1 = p::prepare_input($_POST['slider_text1']);
    $slider_text2 = p::prepare_input($_POST['slider_text2']);
    $slider_group = p::prepare_input($_POST['slider_group']);   
    $slider_sort = (int)$_POST['slider_sort']; 
    $language_id = p::prepare_input($_POST['language_id']);
    $slider_filename = p::prepare_input($_POST['slider_filename']);


    if ( isset($_FILES['slider_img']) and !empty($_FILES['slider_img']['name']))
    {
        $targetFile = DIR_FS_CATALOG.'images/slider/'. $_FILES['slider_img']['name'];
        $tempFile = $_FILES['slider_img']['tmp_name'];
        move_uploaded_file($tempFile, $targetFile);

        if ( is_file($targetFile))
        {
            $slider_filename = $_FILES['slider_img']['name'];

        }
    }  

    if ( !empty($slider_group))
    {
        p::query('insert into slider (slider_url, slider_title, slider_text1, slider_text2, slider_img, slider_group, language_id, slider_sort) values ("'.$slider_url.'", "'.$slider_title.'", "'.$slider_text1.'", "'.$slider_text2.'", "'.p::prepare_input($slider_filename).'", "'.$slider_group.'", "'.$language_id.'", "'.$slider_sort.'");');
    }
    else
    {
        echo 'Заполните обязательные поля';
    }

}

if ( isset($_GET['act']) and $_GET['act'] == 'remove' and isset($_GET['id']))
{ 
    $id= (int) $_GET['id'];
    p::query('delete from slider where slider_id='.$id);
}    

if ( isset($_GET['act']) and $_GET['act'] == 'up')
{
    if ( isset($_POST['sl_url']) and is_array($_POST['sl_url']))
    {
        foreach ($_POST['sl_url'] as $_id=>$_val)
        {
            p::query('update slider set slider_url="'.p::prepare_input($_val).'" where slider_id='.(int)$_id);
        }
    }

    if ( isset($_POST['sl_title']) and is_array($_POST['sl_title']))
    {
        foreach ($_POST['sl_title'] as $_id=>$_val)
        {
            p::query('update slider set slider_title="'.p::prepare_input($_val).'" where slider_id='.(int)$_id);
        }
    }        

}    

$s = p::query('select * from slider order by slider_group, slider_sort');

echo '<table width="100%"><tr><td width="*" valign="top">';
if ( p::num_rows($s) > 0)
{
    echo '<table width="100%" cellspacing="2" cellpadding="2" border="0" class="xr">';
    echo '<tr  class="dataTableHeadingRow" align="center">';
    echo '<td class="dataTableHeadingContent">Картинка</td>';
    echo '<td class="dataTableHeadingContent">Заголовок</td>';
    echo '<td class="dataTableHeadingContent">Группа</td>';
    echo '<td class="dataTableHeadingContent">sort</td>';
    echo '<td class="dataTableHeadingContent">Язык</td>';
    echo '<td class="dataTableHeadingContent">Действие</td>';
    echo '</tr>';


    $str_num = 0;
    $str_val = array();
    $i =0;

    $bs=array();

    while ( $n = p::fetch_array($s) )
    {
        $bs[ $n['slider_id'] ] =  $n; 
    }

    $start = 0;
    if ( isset($_GET['id']))
    {
        if ( isset(  $bs[  $_GET['id']  ]  ))
        {
            $start =  $_GET['id'];
        }
        else
        {
            $start =current( reset($bs) );
        }
    }
    else
    {
        $start =current( reset($bs) );
    }

    foreach ($bs as $m => $n)
    {
        if ( $start == $m )
        {
            echo '<tr align="center" class="dataTableRowSelected" style="background:#FFDAB9">' . "\n";
        }
        else
        {   
            echo '<tr align="center" onclick="document.location.href=\'plugins.php?main_page=slider::page&id='.$n['slider_id'].'\'" onmouseover="this.style.background=\'#e9fff1\';" onmouseout="this.style.background=\''.'#f9f9ff'.'\';"  style="background-color:#f9f9ff';
            echo ';" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'">' . "\n";
        }  

        $_img = '';
        //if (!empty($n['slider_img'])) $_img = '<img src="images/slider/'.$n['slider_img'].'">'; else $_img = 'нет';
        if (!empty($n['slider_img'])) $_img = 'да'; else $_img = 'нет';

        echo '<td>'.$_img.'</td>';

        echo '<td>'.$n['slider_title'].'</td>';
        echo '<td>'.$n['slider_group'].'</td>';
        echo '<td>'.$n['slider_sort'].'</td>';
        echo '<td>'.$am[ $n['language_id']].'</td>';
        echo '<td><a class="reder" href="plugins.php?main_page=slider::page&act=remove&id='.$n['slider_id'].'" onclick="return confirm(\'Действительно хотите удалить отзыв?\')">Удалить</a></td>';
        echo '</tr>';
    }
    echo '</table>';
}
else
{
    echo 'нет добавленных отзывов';
}

echo '</td><td width="350px" valign="top">';

if ( is_array($bs) && count($bs) > 0 && isset($bs[$start]) )
{


    ?>

    <table class="headTable" border="0" cellpadding="2" cellspacing="0" width="97%" align="center">
        <tr>
            <td><b>Редактировать</b></td>
        </tr>
        </tbody></table>
    <?php   

    $data = $bs[$start];
    $lng = '';
    $st = '<select name="language_id">';
    foreach ($am as $_id1 => $_val1)
    {
        if ($data['language_id'] == $_id1)
        {
            $st .= '<option selected value="'.$_id1.'">'.$_val1.'</option>';
        }
        else
        {
            $st .= '<option value="'.$_id1.'">'.$_val1.'</option>';   
        }
    }


$st .='</select>';

echo '<form action="plugins.php?main_page=slider::page&act=update&id='.$data['slider_id'].'" method="post" enctype="multipart/form-data">';
echo '<table class="contentTable" border="0" cellpadding="2" cellspacing="0" width="97%" align="center">';
echo '<tr><td>Заголовок</td><td><input type="text" style="width:100%" name="slider_title" value="'.$data['slider_title'].'"></td></tr>';
echo '<tr><td>Ссылка</td><td><input type="text" style="width:100%" name="slider_url" value="'.$data['slider_url'].'"></td></tr>';
echo '<tr><td>Текст 1</td><td><textarea name="slider_text1" style="width:100%; height:100px">'.$data['slider_text1'].'</textarea></td></tr>';
echo '<tr><td>Текст 2</td><td><textarea name="slider_text2" style="width:100%; height:100px">'.$data['slider_text2'].'</textarea></td></tr>';
echo '<tr><td>Группа</td><td><input type="text" style="width:100%" name="slider_group" value="'.$data['slider_group'].'"></td></tr>';    
echo '<tr><td>Сорт.</td><td><input type="text" style="width:100%" name="slider_sort" value="'.$data['slider_sort'].'"></td></tr>';
echo '<tr><td>Язык</td><td>'.$st.'</td></tr>';
echo '<tr><td>Картинка</td><td><input type="file" name="slider_img"></td></tr>';
echo '<tr><td>Картинка</td><td><input type="text" style="width:100%" name="slider_filename" value="'.$data['slider_img'].'"></td></tr>';
echo '<tr><td></td><td align="right"><button>Сохранить</button></td></tr>';
echo '<table>';
echo '</form><br>';
}

?>
<table class="headTable" border="0" cellpadding="2" cellspacing="0" width="97%" align="center">
    <tr>
        <td><b>Добавить</b></td>
    </tr>
    </tbody></table>
<?php   
$st = '<select name="language_id">';

foreach ($am as $_id1 => $_val1)
 {
    $st .= '<option value="'.$_id1.'">'.$_val1.'</option>';   
} 


$st .='</select>';

echo '<form action="plugins.php?main_page=slider::page&act=add" method="post" enctype="multipart/form-data">';
echo '<table class="contentTable" border="0" cellpadding="2" cellspacing="0" width="97%" align="center">';
echo '<tr><td>Заголовок</td><td><input type="text" style="width:100%" name="slider_title"></td></tr>';
echo '<tr><td>Ссылка</td><td><input type="text" style="width:100%" name="slider_url"></td></tr>';
echo '<tr><td>Текст 1</td><td><textarea name="slider_text1" style="width:100%; height:100px"></textarea></td></tr>';
echo '<tr><td>Текст 2</td><td><textarea name="slider_text2" style="width:100%; height:100px"></textarea></td></tr>';
echo '<tr><td>Группа</td><td><input type="text" style="width:100%" name="slider_group"></td></tr>';
echo '<tr><td>Сорт.</td><td><input type="text" style="width:100%" name="slider_sort"></td></tr>';
echo '<tr><td>Язык</td><td>'.$st.'</td></tr>';
echo '<tr><td>Картинка</td><td><input type="file" name="slider_img"></td></tr>';
echo '<tr><td>Картинка</td><td><input type="text" style="width:100%" name="slider_filename"></td></tr>';
echo '<tr><td></td><td align="right"><button>Добавить</button></td></tr>';
echo '<table>';
echo '</form>';

echo '</td></tr></table>';

