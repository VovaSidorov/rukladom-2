<?php
/*
Plugin Name: Фундамент
Plugin URI: http://160.by/
Version: 1.0
Author: Евгений Фоминов
Author URI: http://160.by/
*/            

add_action('main_page_admin', 'st::page');
add_filter('menu_modules', 'st::menu');
add_filter('product_info_assign', 'st::info');

class st
{
    public static function menu($ar)
    {
        $ar[] =  array(
            'code' => 'st',
            'title' => 'Фундамент',
            'link' => 'plugins.php?main_page=st::page'
        ); 


        return $ar;
    }          

    public static function page()
    {
        include('st.page.php');
    }

    public static function info($ar)
    {
        /*$id = (int)$ar['var']['products_id'];

        $h = 'select * from products_specifications ps, specification_values_description svd where ps.products_id='.$id.' and svd.specification_value=ps.specification and svd.language_id='.$_SESSION['languages_id'].' and  ps.language_id='.$_SESSION['languages_id'];  
        $ar['data']['ch'] = '';
        $n = p::query( $h );
        if ( p::num_rows($n) > 0)
        {
            while ($m = p::fetch_array($n) )
            {    

                if ($m['specifications_id']==24)
                {
                    if ($m['specification_values_id'] == 19)
                    {
                        $ar['data']['ch'] = get_option('st_1');
                         $ar['data']['ch'] = explode("\r",  $ar['data']['ch']);
                    }

                    if ($m['specification_values_id'] == 20)
                    {
                        $ar['data']['ch'] = get_option('st_2');
                        $ar['data']['ch'] = explode("\r",  $ar['data']['ch']);
                    }


                    if ($m['specification_values_id'] == 18)
                    {
                        $ar['data']['ch'] = get_option('st_3');
                        $ar['data']['ch'] = explode("\r",  $ar['data']['ch']);
                    }
                }

            }
        }
        
        */
        
         $ar['data']['ch1'] = get_option('st_1');
         $ar['data']['ch1'] = explode("\r",  $ar['data']['ch1']);     
         
         $ar['data']['ch2'] = get_option('st_2');
         $ar['data']['ch2'] = explode("\r",  $ar['data']['ch2']);         
         
         $ar['data']['ch3'] = get_option('st_3');
         $ar['data']['ch3'] = explode("\r",  $ar['data']['ch3']);

        return $ar;
    }

    public static function install()
    {
        add_option('st_1', '');
        add_option('st_2', '');
        add_option('st_3', '');
    }
} 

?>