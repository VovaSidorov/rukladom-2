<?php
    /*
    Plugin Name: Правила заваривания
    Plugin URI: http://160.by/
    Version: 1.0
    Author: Евгений Фоминов
    Author URI: http://160.by/
    */	
    add_filter('news_product_add_tabs', 'news_product_add_tabs_filter');
    add_action('insert_product', 'insert_product_func');
    add_filter('product_info', 'product_info_func');
    add_action('remove_product', 'remove_product_hang');
    add_action('copy_product', 'hang::_copy');
    
    function remove_product_hang($ar)
    {
        $products_id = $ar['products_id'];
        p::query('delete from hang where products_id='.(int)$products_id);
    }
    
    function product_info_func($ar)
    {  
        if (isset($_GET['products_id']) and !empty($_GET['products_id']))
        {

            $m = p::query('select * from hang where products_id='.(int)$_GET['products_id']);
            if ( p::num_rows($m) >0 )
            {
                $n = p::fetch_array($m);

                $a = array();
                for ($i=1; $i<=10; $i++)
                {

                    if ( !empty( $n['desc'.$i]) )
                    {
                        $a[] = $n['desc'.$i];
                    }			  
                }

                if ( count($a) > 0)
                {
                    $ar['hang'] = $a; 
                }
            }
        }
        return $ar;
    }

    function insert_product_func($ar)
    {
        p::$var['pid'] =  $ar['products_id'];
       
        include('hang.insert.php');
        
        unset(p::$var['pid']);
    }


    function news_product_add_tabs_filter($arr)
    {
        $tab_content = '';
        include('hang.admin.php');

        $arr['values'][] = array(
            'tab_name' => 'Устройство фундамента', 
            'tab_content' => $tab_content,
            'tab_id' =>'hang'
        );

        return $arr;
    }

    function hang_install()
    {
        p::query("CREATE TABLE IF NOT EXISTS hang (
            hang_id int NOT NULL auto_increment,
            products_id int(7) DEFAULT '0',
            desc1 varchar(255) NOT NULL,
            desc2 varchar(255) NOT NULL,
            desc3 varchar(255) NOT NULL,
            desc4 varchar(255) NOT NULL,
            desc5 varchar(255) NOT NULL,            
            desc6 varchar(255) NOT NULL,
            desc7 varchar(255) NOT NULL,
            desc8 varchar(255) NOT NULL,
            desc9 varchar(255) NOT NULL,
            desc10 varchar(255) NOT NULL,
            PRIMARY KEY  (hang_id)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;");
    }

    function hang_remove()
    {
        p::query('drop table if exists hang;');

    }
    
    class hang 
    {
        public static function _copy($ar)
        {
            $m = p::query('select * from hang where products_id='.$ar['products_id']);
            if ( p::num_rows($m) >0 )
            {
                $n = p::fetch_array($m);
                unset($n['hang_id']);
                $n['products_id'] = $ar['new_products_id'];
                p::perform('hang', $n);
            }
            
        }
    }
?>