<?php
/*
Plugin Name: Параметры в листинге товара
Plugin URI: http://160.by/
Version: 1.1
Author: Евгений Фоминов
Author URI: http://160.by/
*/    

add_filter('product_list', 'get_spec_categories');
add_filter('product_info_assign', 'product_info_func7');
add_filter('buildDataArray', 'buildDataArray7');
/*
data - товары
s = array( var => array(), data=>array())
*/


function buildDataArray7($ar)
{
    $id = $ar['PRODUCTS_ID'];

    $h = 'select * from products_specifications ps, specification_values_description svd where ps.products_id='.$id.' and svd.specification_value=ps.specification and svd.language_id='.$_SESSION['languages_id'].' and  ps.language_id='.$_SESSION['languages_id'];  
    $n = p::query( $h );
    if ( p::num_rows($n) > 0)
    {
        while ($m = p::fetch_array($n) )
        {                  
            if ($m['specifications_id']==22)
            {
                $ar['text'] = $m['specification_values_id'];
            }

        }
    }

    $h = 'select * from products_specifications where products_id='.$id.' and  language_id='.$_SESSION['languages_id'];

    $n = p::query( $h );
    if ( p::num_rows($n) > 0)
    {
        while ( $m = p::fetch_array($n) )
        {
            if ($m['specifications_id'] == 21  )
            {
                $ar['sh'] = $m['specification']; 
            }           

            if ($m['specifications_id'] == 22  )
            {
                $ar['mk'] = $m['specification']; 
            }

        }  

    }      

    return $ar;  
}

function product_info_func7($ar)
{
    $id = $ar['var']['products_id'];
    $products_price = $ar['var']['products_price'];
    $h = 'select * from products_specifications where products_id='.$id.' and specifications_id=21 and language_id='.$_SESSION['languages_id'];

    $n = p::query( $h );
    if ( p::num_rows($n) > 0)
    {
        $m = p::fetch_array($n);
        $ar['data']['sh'] = $m['specification'];
        $m['specification'] = (int)$m['specification'];
        if ($m['specification'] !=0)
        {
            $ar['data']['ppr'] = round($products_price/$m['specification']);
        }
    }


    $h = 'select * from products_specifications ps, specification_values_description svd where ps.products_id='.$id.' and svd.specification_value=ps.specification and svd.language_id='.$_SESSION['languages_id'].' and  ps.language_id='.$_SESSION['languages_id'];  
    $n = p::query( $h );
    if ( p::num_rows($n) > 0)
    {
        while ($m = p::fetch_array($n) )
        {                  
            if ($m['specifications_id']==22)
            {
                $ar['data']['text'] = $m['specification_values_id'];
            }

        }
    }

    global $vamPrice;
    $m = p::query('select * from products where products_id='.$id);
    $n = p::fetch_array($m);

    $products_price0  = $n['products_price0'];

    $ecom ='';
    if ($products_price0 == 0) {
        $products_price0= '';
    } else { 

        if (  $products_price0 - $products_price > 0) {
            $ecom =  $products_price0 - $products_price;
            $ecom = $vamPrice->Format($ecom, true);
            $ecom = str_replace('<span class="thousands"></span>',' ', $ecom);
        }

        $products_price0 = $vamPrice->Format($products_price0, true);
        $products_price0 = str_replace('<span class="thousands"></span>',' ', $products_price0);
    }

    $ar['data']['PRODUCTS_PRICE0'] =  $products_price0;
    $ar['data']['PRODUCTS_ECOM'] =  $ecom;
    return $ar;    
}

function get_spec_categories($s)
{
    $v = $s['data'];  		

    $category_id = $v['var']['category_id'];

    if ( is_array($v) and count($v) > 0)
    {
        $pid = array();

        foreach ($v as $key => $val)
        {
            $pid[] = $val['PRODUCTS_ID'];
        }

        if ( is_array($pid) and count($pid) >0 ){}else return $s;

        $str_q = implode(',', $pid);

        $str = "select *                                     
        from products_specifications ps, 
        specifications s, 
        specification_description sd, 
        specification_groups sg,
        specification_groups_to_categories sg2c
        where sg.show_products = 'True'
        and s.show_products = 'True'
        and ps.language_id=".$_SESSION['languages_id']."
        and sd.language_id=".$_SESSION['languages_id']."
        and s.specification_group_id = sg.specification_group_id 
        and sg.specification_group_id = sg2c.specification_group_id
        and sd.specifications_id = s.specifications_id
        and ps.specifications_id = sd.specifications_id
        and sg2c.categories_id = '".$category_id."' 
        and ps.products_id in (".$str_q.")  
        order by s.specification_sort_order, 
        sd.specification_name";

        $n = p::query($str );
        $ider = array();


        while ($m = p::fetch_array($n))
        {
            if ( !isset( $ider[ $m['products_id'] ] ) )  $ider[ $m['products_id'] ] = array();
            $ider[ $m['products_id'] ][] =    $m;

        }  
        foreach ($v  as $num=>$val)
        {
            if ( isset( $ider[  $val['PRODUCTS_ID']  ] ) )
            {
                if ( !isset( $v[$num ]['spec'] ))  $v[$num ]['spec'] = array();
                $v[$num ]['spec'] =    $ider[  $val['PRODUCTS_ID']  ];

                foreach ($v[$num ]['spec'] as $id7=>$val7)
                {
                    if ($val7['specifications_id'] == 21)
                    {
                        $v[$num ]['sh'] =  $val7['specification'];
                    }
                }
            }  
        }

    }	

    $s['data'] = $v;				
    return $s;									
}
?>