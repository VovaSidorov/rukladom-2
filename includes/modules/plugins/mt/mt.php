<?php
/*
Plugin Name: Комплектация дома
Plugin URI: http://160.by/
Version: 1.0
Author: Евгений Фоминов
Author URI: http://160.by/
*/            

add_action('main_page_admin', 'mt::page');
add_filter('menu_modules', 'mt::menu');
add_filter('product_info_assign', 'mt::info');

class mt
{
    public static function menu($ar)
    {
        $ar[] =  array(
            'code' => 'mt',
            'title' => 'Комплектация дома',
            'link' => 'plugins.php?main_page=mt::page'
        ); 


        return $ar;
    }          

    public static function page()
    {
        include('mt.page.php');
    }

    public static function info($ar)
    {
        $id = (int)$ar['var']['products_id'];

         $ar['data']['mt'] = get_option('mt_1');
      
        return $ar;
    }

    public static function install()
    {
        add_option('mt_1', '');
    }
} 

?>