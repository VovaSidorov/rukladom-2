<?php
if ( isset($_GET['type']) and $_GET['type']=='ajax')
{
    add_action('header_start', 'header_start_func');
    add_action('header_end', 'header_end_func');
    add_action('product_list', 'product_list_ajax');
    add_action('product_list_empty', 'product_list_empty_filter');
}

add_filter('product_listing_sql', 'product_listing_sql_func');
add_filter('main_assign', 'func_add');

function func_add($ar)
{
    $_max =  null;
    $_min =  null;
    $listing_query = p::query('SELECT * FROM `products`');
    while ($listing = p::fetch_array($listing_query, true)) {
        $rows ++;

        if ( $_max < $listing['products_price'] or is_null($_max) ) $_max = $listing['products_price'];
        if ( $_min > $listing['products_price'] or is_null($_min) ) $_min = $listing['products_price'];      
    } 
    $ar['max'] = round($_max);
    $ar['min'] = round($_min);
    return $ar;    
}

function product_listing_sql_func($ar)
{
    p::$var['filter']['sql']= $ar; 
    return $ar;
}

add_filter('product_listing_assign', 'product_list_filter');

function header_start_func()
{
    ob_start();
}


function product_list_empty_filter()
{
    //  $am['val'] = '<center>Нет товаровы<br><a onclick="drop_filter()">Сброс</a></center>';
    $am['val'] = '    <div class="rL hid no_found_rez alCenter">
    <h3>Нет подходящих товаров</h3>
    <p>попробуйте смягчить условия поиска</p>
    <button class="no_param"  onclick="drop_filter();return false;"><span>Сбросить параметры</span></button>
    </div>';
    $am['count'] = 0;
    $am['check'] = array();
    $am['max'] = '';
    $am['min'] = '';
    $am['man'] = array();
    $am['mm'] = array();
    echo json_encode($am);

    die();
}

function header_end_func()
{
    $m_content = ob_get_contents();
    ob_end_clean();
}

function product_list_ajax($module_content)
{
    $am = array();

    $module = new vamTemplate;


    if (isset($_GET['sort']) && $_GET['sort']=='price_tech')
    {
        if ( is_array( $module_content) && count( $module_content) > 0)
        {
            $op = array();
            //specifications_id
            foreach ($module_content as  $m => $v)
            {
                if ( !empty($v['spec']))
                {
                    foreach ($v['spec'] as $_m =>$_v)
                    {
                        if ($_v['specifications_id'] == 22)
                        {
                            $module_content[$m]['t'] = $_v['specification']; 
                            $op[$m] = $_v['specification'];
                        }
                    }
                }
            }



            if ( $_GET['direction'] == 'desc')
            {
                asort($op, SORT_LOCALE_STRING);  
            }
            else
            {
                arsort($op, SORT_LOCALE_STRING);   
            }


            $_s = array(); 
            if (!empty($op))
            {
                foreach ($op as $id=>$val)
                {
                    $_s[] = $module_content[$id];  
                }

                $module->assign('module_content', $_s);
            } 
        }


    }elseif (isset($_GET['sort']) && $_GET['sort']=='price_s')
    {
        if ( is_array( $module_content) && count( $module_content) > 0)
        {
            $op = array();
            //specifications_id
            foreach ($module_content as  $m => $v)
            {
                $op[$m] = (int)$v['sh'];

            }

            if ( $_GET['direction'] == 'desc')
            {
                arsort($op); 
            }
            else
            {
                
                asort($op);    
            }
            

            $_s = array(); 
            if (!empty($op))
            {
                foreach ($op as $id=>$val)
                {
                    $_s[] = $module_content[$id];  
                }

                $module->assign('module_content', $_s);
            } 
        }
    }
    else
    {
        $module->assign('module_content', $module_content);
    }

    $am['val'] = $module->fetch(CURRENT_TEMPLATE.'/ajax_product_listing.html');
    $am['count'] = count($module_content);

    $man = array();
    $n = p::query( p::$var['filter']['sql']);
    if ( p::num_rows($n))
    {
        $pid = array();
        while  ($m = p::fetch_array($n))
        {
            $pid[] = $m['products_id']; 
        }

        $str = implode(',', $pid); 
        $q2 = "select *, 
        m.manufacturers_id as mid, 
        m.manufacturers_name as mname                                   
        from  
        products_specifications ps, 
        specifications s, 
        specification_description sd, 
        specification_groups sg,
        specification_groups_to_categories sg2c,
        products p
        left join manufacturers m on p.manufacturers_id = m.manufacturers_id   
        where 
        p.products_id = ps.products_id and
        sg.show_products = 'True'
        and s.show_products = 'True'
        and ps.language_id=".$_SESSION['languages_id']."
        and sd.language_id=".$_SESSION['languages_id']."
        and s.specification_group_id = sg.specification_group_id 
        and sg.specification_group_id = sg2c.specification_group_id
        and sd.specifications_id = s.specifications_id
        and ps.specifications_id = sd.specifications_id
        and p.products_id in (".$str.")
        order by s.specification_sort_order, 
        sd.specification_name";


        $x = p::query( $q2);

        $s = array();
        $_max = '';
        $_min = '';        

        $_max2 = '';
        $_min2 = '';
        //ма
        $_mm = array();

        while ($w = p::fetch_array($x))
        {
            if ( $w['filter_display'] == 'creep' )  
            {
                if ( !isset( $_mm[ $w['specifications_id'] ]  )) $_mm[ $w['specifications_id'] ] = array('max'=> '', 'min'=>'');

                //     if ( $_mm[ $w['specifications_id'] ]['min'] )

                $__max =  $_mm[ $w['specifications_id'] ]['max'];

                if (!empty($__max)) 
                {
                    if ($__max < $w['specification'] ) $__max = $w['specification'];  
                }
                else $__max = $w['specification']; 

                $_mm[ $w['specifications_id'] ]['max'] = $__max;

                $__min =  $_mm[ $w['specifications_id'] ]['min']; 
                if (!empty($__min)) 
                {
                    if ($__min > $w['specification']) $__min = $w['specification']; 
                }
                else $__min = $w['specification']; 

                $_mm[ $w['specifications_id'] ]['min'] = $__min;



            }

            if (!is_null($w['mid']))
            {
                $man[ $w['mid']] =$w['mname'];
            }

            $name = 'a'.$w['specifications_id'].'_'.$w['specification'];

            if (!empty($_max)) 
            {
                if ($_max < $w['products_price']) $_max = $w['products_price'];  
            }
            else $_max = $w['products_price']; 


            if (!empty($_min)) 
            {
                if ($_min > $w['products_price']) $_min = $w['products_price']; 
            }
            else $_min = $w['products_price']; 

            if ( !isset( $s[ $name ]  ) ) $s[ $name ] =  1;

            if ($w['specifications_id'] == 21)
            {
                if (!empty($__min2)) 
                {
                    if ($__min2 > $w['specification']) $__min2 = $w['specification']; 
                }
                else $__min2 = $w['specification'];

                if (!empty($_max2)) 
                {
                    if ($_max2 < $w['specification']) $_max2 = $w['specification'];  
                }
                else $_max2 = $w['specification']; 
            }
        }



        $am['check'] = $s;
        $am['max'] = round($_max);
        $am['min'] = round($_min);     
        $am['max2'] = round($_max2);
        $am['min2'] = round($_min2);


        //  $am['man'] = $man;
        $am['mm'] = $_mm;

    }

    echo json_encode($am);

    die();
} 

function product_list_filter($ar)
{      

    p::$var['category_id'] = $ar['var']['category_id'];

    //  ob_start();

    include('filter.menu.php');


    return $ar;
}

function filter_install()
{
    add_option('mon_vis', 'true', 'checkbox', "array('true', 'false')");
}


function get_max_array($arr)
{
    $max = $arr[0];
    foreach ($arr as $val)
    {
        if ($max < $val) $max = $val;
    }
    return $max;
}

function get_min_array($arr)
{
    $min = $arr[0];
    foreach ($arr as $val)
    {
        if ($min > $val) $min = $val;
    }
    return $min;
}
?>