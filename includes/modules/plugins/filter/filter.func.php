<?php
    function get_cat_id($id)
    {
        if (!isset(p::$var['cat']))
        {
            $n = p::query('select * from categories;');
            while ($m = p::fetch_array($n))
            {
                p::$var['cat'][$m['categories_id']]  = $m['parent_id'];
            }
        }

        $r = array();
        $r[] = $id;
        
        if ( is_array(p::$var['cat']) and count(p::$var['cat']) > 0 )
        {
            foreach (p::$var['cat'] as $_cat_id => $_parent_id)
            {
                 if ($_parent_id == $id)
                 {
                     $r[] =  $_cat_id;
                 }
            }
        }
        
        return implode(',', $r);

    }
?>  