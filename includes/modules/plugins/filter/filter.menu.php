<?php
include('filter.func.php');
$category_id = p::$var['category_id'];

$_sql =  get_cat_id($category_id) ;
$g = "select s.specifications_id,
s.products_column_name,
s.filter_class,
s.filter_show_all,
s.filter_display,
sd.specification_name,
sd.specification_prefix,
sd.specification_suffix
from specifications s,
specification_description sd,
specification_groups sg,
specification_groups_to_categories s2c
where s.specification_group_id = sg.specification_group_id
and sd.language_id=".$_SESSION['languages_id']."
and sg.specification_group_id = s2c.specification_group_id
and sd.specifications_id = s.specifications_id
and s2c.categories_id in ('".$_sql."')
and s.show_filter = 'True'
and sg.show_filter = 'True'
and sd.language_id = '1'
order by s.specification_sort_order,
sd.specification_name";


$n = p::query($g);
$f_name = array();
while ($m = p::fetch_array($n))
{
    // if ( $m['filter_class'] == 'exact')
    // {
    $f_name[] = $m;
    //}
}


/*   $q2 = "select p.products_price, filter_class, ps.specification, ps.products_id,
s.filter_display,
s.enter_values,
sd.specification_name, 
sd.specifications_id,
sd.specification_prefix, 
sd.specification_suffix,
s.specification_group_id,
sg.specification_group_name                                      
from products p, 
products_specifications ps, 
specifications s, 
specification_description sd, 
specification_groups sg,
specification_groups_to_categories sg2c
where 
p.products_id = ps.products_id and
sg.show_products = 'True'
and s.show_products = 'True'
and s.specification_group_id = sg.specification_group_id 
and sg.specification_group_id = sg2c.specification_group_id
and sd.specifications_id = s.specifications_id
and ps.specifications_id = sd.specifications_id
and sg2c.categories_id = ".$category_id."
order by s.specification_sort_order, 
sd.specification_name";
*/
$s = 'SELECT *
FROM products_to_categories pc, products p
WHERE pc.categories_id
IN ( '.$_sql.' )
AND pc.products_id = p.products_id';

$n = p::query($s);
$p_id = array();
$_max ='';
$_min = '';    
while ($m = p::fetch_array($n))
{
    if (!empty($_max)) 
    {
        if ($_max < $m['products_price']) $_max = $m['products_price'];  
    }
    else $_max = $m['products_price']; 

    if (!empty($_min)) 
    {
        if ($_min > $m['products_price']) $_min = $m['products_price']; 
    }
    else $_min = $m['products_price']; 
}  

//   $p_id_sql = implode('');
//  print_r($p_id);  


$q2 = 'select * , m.manufacturers_id as mid, m.manufacturers_name as mname from products_to_categories pc, 
products_specifications ps, 
specifications s, 
specification_description sp, 
products p
left join manufacturers m on p.manufacturers_id = m.manufacturers_id 
where  pc.categories_id in ('.$_sql.') 
and pc.products_id=ps.products_id 
and ps.language_id='.$_SESSION['languages_id'].'
and s.specifications_id=ps.specifications_id 
and sp.specifications_id=ps.specifications_id 
and pc.products_id=p.products_id';

$n = p::query($q2);
$s_value = array();
$x_value = array();

$p_str = '';
$_sort = '';
$___max = ''; 
$___min = ''; 

if ( is_array($_GET) and count($_GET) > 0)
{
    $p = array();
    $_man = array();
    $_f = array();

    foreach ($_GET as $name => $val)
    {
        if ($name != 'type' and $name!='start' and $name!='p' and $name !='sort' and $name != 'direction' and $name !='max' and $name !='min')
        {
            if (!is_array($val) and mb_substr($name, 0, 1) !='f' and $name !='m')
            {
                if ( empty($p_str) )  $p_str .= $name.'='.$val; else $p_str .= '&'.$name.'='.$val;
            }

            if ( mb_substr($name, 0, 1) =='f' )
            {
                $_n = str_replace('f','', $name);
                if (!is_array($val))
                {
                    $name = strip_tags($name);
                    $val =  strip_tags($val);

                    $p[$_n] = $val;
                }
                else
                {
                    $p[$_n] = $val;
                }
            }

            if ($name == 'm' and is_array($val))
            {
                foreach ($val as $___i => $___val)
                {
                    $_man[$___val]=1;
                }
            }
        }

        if ( $name =='sort' or $name == 'direction')
        {
            $_sort .= '&'.$name.'='.$val;
        }

        if ( $name =='min' )
        {
            $___min = $val;
        } 

        if ( $name =='max' )
        {
            $___max = $val;
        }
    }
}

$__min2 = '';
$__max2  = '';

$man = array();
$g1 = array();
while ($m = p::fetch_array($n))
{
    if (!is_null($m['mid']))
    {
        $man[ $m['mid']] =$m['mname'];
    }

    if ( !isset($s_value[ $m['specifications_id']] )) $s_value[ $m['specifications_id']]  = array();
    if ( !isset($s_value[ $m['specifications_id']][ $m['specification']] )) $s_value[ $m['specifications_id']][$m['specification']]  = array();
    $s_value[ $m['specifications_id']][ $m['specification'] ] = $m;

    if ( !isset($x_value[ $m['specifications_id']] )) $x_value[ $m['specifications_id']]  = array();

    if ( !in_array($m['specification'], $x_value[ $m['specifications_id']]))
    {
        $x_value[ $m['specifications_id']][] = $m['specification'];
    }

    if ($m['specifications_id'] == 21)
    {
        $g1[] =(int)$m['specification']; 
        if (!empty($__min2)) 
        {
            if ($__min2 > $m['specification']) $__min2 = $m['specification']; 
        }
        else $__min2 = $m['specification'];
        
        if (!empty($__max2)) 
        {
            if ($__max2 < $m['specification']) $__max2 = $m['specification'];  
        }
        else $__max2 = $m['specification']; 
    }
}


$__min2 = get_min_array($g1);
$__max2 = get_max_array($g1);


if ( is_array($x_value) and count($x_value) >0)
{
    foreach ($x_value as $key=>$val)
    {
        sort($x_value[$key]);
    }

}


$tpl_m = array();
//тип полей
$tpl_type = array();
if ( is_array($f_name) and count($f_name) >0)
{	
    foreach ($f_name as $num =>$val)
    {
        //   echo $val['specification_name'].'<br>';

        if ( isset($s_value [ $val['specifications_id'] ]))
        {
            if ( !isset( $tpl_m[ $val['specification_name'] ] )) $tpl_m[ $val['specification_name'] ] = array();
            $k = $x_value [ $val['specifications_id'] ];
            $i=0;
            $tpl_type[ $val['specification_name'] ]  = $val['filter_display'];



            if ( $val['filter_display'] == 'checkbox')
            { 
                foreach ($k as $_k => $_v)
                {

                    $name = trim($_v);

                    $tpl_m[ $val['specification_name'] ] [] = array('name'=>$name, 
                        'id'=>$val['specifications_id'], 
                        'i'=>$i,
                        'suffix'=>$val['specification_suffix']);   
                    //  echo '<input type="checkbox" value="2" name="f'.$val['specifications_id'].'[1]">'.$_v.'<br>';

                    $i++;


                }

            }
            else if ($val['filter_display'] == 'creep')
            {
                $__min = '';
                $__max = '';
                foreach ($k as $_k => $_v)
                {
                    $_num = (int)trim($_v);

                    if (!empty($__max)) 
                    {
                        if ($__max < $_num) $__max = $_num;  
                    }
                    else $__max = $_num; 

                    if (!empty($__min)) 
                    {
                        if ($__min > $_num) $__min = $_num; 
                    }
                    else $__min = $_num; 
                }

                $param_min = '';
                $param_max = '';
                if (isset($p[ $val['specifications_id'] ]))
                {
                    $m =  explode('-', $p[ $val['specifications_id']] );
                    if ( count($m) ==2)
                    {
                        $param_min = $m[0];
                        $param_max = $m[1];
                        if($param_max == 0) $param_max = '';
                        if($param_min == 0) $param_min = '';
                    }
                }

                $tpl_m[ $val['specification_name'] ] = array(
                    'min'=> $__min,
                    'max'=> $__max,
                    'param_min' => $param_min,
                    'param_max' => $param_max,
                    'name'=>$val['specification_name'],
                    'suffix'=>$val['specification_suffix'],
                    'id'=>$val['specifications_id'], 

                );
            }
        }

    }
}	





$module = new vamTemplate;
$module->assign('fil', $tpl_m);
$module->assign('fil_type', $tpl_type);
$module->assign('min', round($_min));

//  if ( get_option('mon_vis') == 'true')
// {
$module->assign('man', $man);
$module->assign('param_man', $_man);
// }

$module->assign('max', round($_max));
$module->assign('param_array', $p);
$module->assign('param_min', $___min);
$module->assign('param_max', $___max);
$module->assign('param_min2', round($__min2) );
$module->assign('param_max2', round($__max2) );
$module->assign('param_sort', $_sort);

if ( isset($_GET['start'])) $module->assign('param_start', (int)$_GET['start']);
$module->assign('language', $_SESSION['language']);
$module->assign('param_str', $p_str.'&on_page=100');
$module = $module->fetch(CURRENT_TEMPLATE.'/filter.html');
$ar['data']['filt'] = $module;
?>