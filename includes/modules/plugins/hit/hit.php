<?php
/*
Plugin Name: Хиты продаж
Plugin URI: http://160.by/
Version: 1.0
Author: Евгений Фоминов
Author URI: http://160.by/
*/		

add_filter('center_modules',     'hit::info');
add_filter('categories_switch_name',  'hit::name');
add_filter('categories_switch_value', 'hit::value');
add_action('categories_action',       'hit::action');

class hit 
{
    public static function action()
    {
        if (isset($_GET['action']) and  $_GET['action']=='setpflag_hit' and isset($_GET['pID'])) 
        {
            $id = (int)$_GET['pID'];
            $flag = (int)$_GET['flag'];

            p::query('update products set hit="'.$flag.'" where products_id='.$_GET['pID']);  
        }
    }

    public static function name($ar)
    {
        $ar[] = 'Xит';
        return $ar;
    }	   

    public static function info($ar)
    {
        global $product;
        $a = "SELECT distinct * FROM
        ".TABLE_PRODUCTS." p,
        ".TABLE_PRODUCTS_DESCRIPTION." pd,
        ".TABLE_PRODUCTS_TO_CATEGORIES." p2c,
        ".TABLE_CATEGORIES." c
        where c.categories_status='1' and p.hit=1
        and p.products_id = p2c.products_id and p.products_id=pd.products_id
        and p2c.categories_id = c.categories_id
        and p.products_status = '1' and pd.language_id = '".(int) $_SESSION['languages_id']."'
        group by p.products_id
        order by p.products_sort, p.products_id DESC;";
        $s = p::query($a);
        $am = array();
        $module_content = array ();
        $new_products_query = vamDBquery($a);
        while ($new_products = vam_db_fetch_array($new_products_query, true)) {
            $module_content[] = $product->buildDataArray($new_products);

        }
        $ar['hit'] = $module_content;
        return $ar;
    }

    //установка плагина
    public static function install()
    {
        p::query('ALTER TABLE `products` ADD `hit` int(11) NOT NULL');
    }

    //удаление плагина
    public static function remove()
    {
        p::query('ALTER TABLE `products` DROP `hit`;');
    }

    public static function value($ar)
    {
        $content = '';
        $opt = $ar['option'];
        $pre = $opt['hit'];
        $id = $opt['products_id'];
        $categories_id= $opt['categories_id'];

        $con = '';
        if ($pre == 1)
        {
            $con .= '<a style="padding:3px;"><img src="/'.p::$dir.'img/icon_status_green.gif"></a>';
            $con .= '<a style="padding:3px;" href="categories.php?action=setpflag_hit&flag=0&pID='.$id.'&cPath='.$categories_id.'"><img src="/'.p::$dir.'img/icon_status_red_light.gif"></a>';
        }
        else
        {
            $con .= '<a style="padding:3px;" href="categories.php?action=setpflag_hit&flag=1&pID='.$id.'&cPath='.$categories_id.'"><img src="/'.p::$dir.'img/icon_status_green_light.gif"></a>';
            $con .= '<a style="padding:3px;"><img src="/'.p::$dir.'img/icon_status_red.gif"></a>';
        }

        $ar['values'][] = $con;
        return $ar;
    }
}
?>