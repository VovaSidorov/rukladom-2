<?php
/*
Plugin Name: Комплектация товара
Plugin URI: http://160.by/
Version: 0.1
Author: Евгений Фоминов
Author URI: http://160.by/
*/		
add_filter('news_product_add_tabs', 'news_product_add_tabs_options');

//remove_product
//insert_product

add_action('insert_product', 'insert_product_options');
add_action('remove_product', 'remove_product_options');
add_filter('product_info', 'product_info_options');
add_filter('print_product_info', 'product_info_options');

add_action('copy_product', 'copy_options');

function copy_options($ar)
{
    $m = p::query('select * from options where products_id='.$ar['products_id']);
    if ( p::num_rows($m) >0 )
    {
        $n = p::fetch_array($m);
        unset($n['options_id']);
        $n['products_id'] = $ar['new_products_id'];
        p::perform('options', $n);
    } 
}

function remove_product_options($ar)
{
    $products_id = $ar['products_id'];
    p::query('delete from options where products_id='.(int)$products_id);
}

function insert_product_options($ar)
{
    $products_id = $ar['products_id'];

    $pid = $products_id;

    $a = array();
    if ( isset($_POST['options_value']))
    { 
        $a['value'] = p::prepare_input($_POST['options_value']);

        $m = p::query('select * from options where products_id='.(int)$pid);
        if ( p::num_rows ($m)>0)
        {
            p::perform('options', $a, 'update', 'products_id='.(int)$pid);
        }
        else
        {
            $a['products_id']= $pid;
            p::perform('options', $a, 'insert');
        }
    }
    // include('hang.insert.php');
}

function product_info_options($ar)
{
    $m = p::query('select * from options where products_id='.(int)$_GET['products_id']);
    if ( p::num_rows($m) >0 )
    {
        $n = p::fetch_array($m);

        if ( !empty($n['value'] )) 
        {
            $ar['opti'] = $n['value'];
        }
    }

    return $ar;
}

function news_product_add_tabs_options($arr)
{
    $tab_content = '';
    include('options.admin.php');

    $arr['values'][] = array(
        'tab_name' => 'Комплектация дома', 
        'tab_content' => $tab_content,
        'tab_id' =>'optionss'
    );

    return $arr;
}

function options_install()
{
    p::query("CREATE TABLE IF NOT EXISTS options (
        options_id int NOT NULL auto_increment,
        products_id int(7) DEFAULT '0',
        value varchar(255) NOT NULL,

        PRIMARY KEY  (options_id)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;");
}

function options_remove()
{
    p::query('drop table if exists options;');

}
?>