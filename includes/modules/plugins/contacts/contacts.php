<?php
/*
Plugin Name: Контакты
Plugin URI: http://160.by/
Version: 1.1
Author: Евгений Фоминов
Author URI: http://160.by/
*/    


add_action('main_page_admin', 'contacts::page');
add_filter('menu_modules', 'contacts::menu');

class contacts
{
    public function menu($arr)
    {
        $arr[] =  array(
            'code' => 'rev',
            'title' => 'Контакты',
            'link' => 'plugins.php?type=1&module=contacts'
        ); 

        return $arr;  
    } 

    public function install()
    {
       // Адреса головного офиса
       add_option('contacts_1', '191186, ул. Миллионная, д. 11, оф. 114', '', '', 1); 
       add_option('contacts_2_1', '+7 (495) 123 45 67 доб. 8', '', '', 1); 
       add_option('contacts_2_2', 'buyme3@ruklad.ru', '', '', 1);        
       
       add_option('contacts_3_1', '+7 (495) 123 45 67 доб. 9', '', '', 1); 
       add_option('contacts_3_2', 'buyme2@ruklad.ru', '', '', 1); 
       
       add_option('contacts_4_1', '+7 (495) 123 45 67 доб. 10', '', '', 1); 
       add_option('contacts_4_2', 'buyme1@ruklad.ru', '', '', 1); 
       
       add_option('contacts_5_1', '+7 (495) 123 45 67 доб. 11', '', '', 1); 
       add_option('contacts_5_2', 'buyme5@ruklad.ru', '', '', 1); 

    } 
}
?>