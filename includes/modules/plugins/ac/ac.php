<?php
/*
Plugin Name: Акции
Plugin URI: http://160.by/
Version: 1.0
Author: Евгений Фоминов
Author URI: http://160.by/
*/            

add_action('main_page_admin', 'ac::page');
add_filter('menu_modules', 'ac::menu');
add_filter('center_modules', 'ac::info');

class ac
{
    public static function menu($ar)
    {
        $ar[] =  array(
            'code' => 'ac',
            'title' => 'Акции',
            'link' => 'plugins.php?main_page=ac::page'
        ); 


        return $ar;
    }          

    public static function page()
    {
        include('ac.page.php');
    }

    public static function info($ar)
    {
        $s = get_option('act_day');
        if ( !empty($s))
        { 
            $m =  explode('/', $s) ;
            $dd = mktime(0,0,0, $m[1], $m[2], $m[0]);
            $t = time();
            if ($dd > $t)
            {
                global $product;
                $k = $dd-$t;
                $ar['ac_time'] = $k; 
                $ar['act_desc'] = get_option('act_desc');

                $new_products_query = "SELECT distinct * FROM
                ".TABLE_PRODUCTS." p,
                ".TABLE_PRODUCTS_DESCRIPTION." pd WHERE
                p.products_id=pd.products_id  and p.products_id=".get_option('act_id')."
                and p.products_status = '1' and pd.language_id = '".(int) $_SESSION['languages_id']."' limit 1";

                $new_products_query = vamDBquery($new_products_query);
                $new_products = vam_db_fetch_array($new_products_query, true) ;

                $s = $product->buildDataArray($new_products);   
                $ar['ac_image'] = $s['PRODUCTS_IMAGE'];                       
            }
        }

        return $ar;
    }

    public static function install()
    {
        add_option('act_id', '0');
        add_option('act_day', '');
        add_option('act_desc', '');
    }
} 

?>