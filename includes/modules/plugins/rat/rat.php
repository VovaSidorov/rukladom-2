<?php
    /*
    Plugin Name: Опрос 
    Plugin URI: http://160.by/
    Version: 1.2
    Author: Евгений Фоминов
    Author URI: http://160.by/
    */
	
    add_filter('menu_modules',    'rat::menu');
    add_filter('page',    'rat::page2');
    add_action('main_page_admin', 'rat::page');
    add_filter('main_assign',     'rat::info');
    add_filter('center_modules',  'rat::info');
    add_filter('product_info',  'rat::info');
    add_filter('product_listing',  'rat::info');

    class rat
	{
       public static function page()
       {
          include('rat.page.php');
       }	   
       
       public static function page2()
	   {
	      include('rat.page2.php');
	   }
	   
	   public static function remove()
	   {
	      p::query("drop table rat;");
	   }
	   
	   public static function install()
	   {
	        p::query("CREATE TABLE IF NOT EXISTS rat (
            rat_id int NOT NULL auto_increment,
            rat_url varchar(255) NOT NULL,
            rat_img varchar(255) NOT NULL,
            rat_sort int NOT NULL,
            language_id int NOT NULL,
            rat_title varchar(255) NOT NULL,
            rat_text1 varchar(255) NOT NULL,
            rat_text2 varchar(255) NOT NULL,
            rat_group varchar(255) NOT NULL,
            PRIMARY KEY  (rat_id)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;");

        add_option('rat_status', 'true', 'radio', 'array("true", "false")');
	   }
	   
	   public static function menu($arr)
	   {
	      $arr[] =  array(
            'code' => 'rat',
            'title' => 'Анкета',
            'link' => 'plugins.php?main_page=rat::page'
           ); 
		   
	      return $arr;
	   }   
	   
       public static function info($ar)
       {
	       $group = array();

           $s = p::query('select * from rat where language_id='.$_SESSION['languages_id'].' or language_id=0 order by rat_sort');
           while ($n = p::fetch_array($s))
           {
		      if ( !isset($group [ $n['rat_group'] ] )) $group [ $n['rat_group'] ]  = array();
		      if ( !empty($n['rat_group']))
			  {
			     $group [ $n['rat_group'] ][]  = $n;
			  }
           } 

		   if ( is_array($group) && count($group) > 0)
		   {
		      foreach ($group as $name=>$val)
			  {
			     $ar[ $name ] = $val; 
			  }
		   }
         
           return $ar;		   
       }
	   
	   //osc
	   public static function info2($_group_name)
       {
	       $ar = array();
	       $group = array();
           $s = p::query('select * from rat order by rat_sort');
           while ($n = p::fetch_array($s))
           {
		      if ( !isset($group [ $n['rat_group'] ] )) $group [ $n['rat_group'] ]  = array();
		      if ( !empty($n['rat_group']))
			  {
			     $group [ $n['rat_group'] ][]  = $n;
			  }
           } 

		   if ( is_array($group) && count($group) > 0)
		   {
		      foreach ($group as $name=>$val)
			  {
			     $ar[ $name ] = $val; 
			  }
		   }

		   if ( isset( $ar [ $_group_name ] ))
		   {
		      return $ar [ $_group_name ];
		   }		   
       }
	
	}



?>