<?php
/*
Plugin Name: Сжатие html кода
Plugin URI: http://160.by/
Version: 1.0
Author: Евгений Фоминов
Author URI: http://160.by/
*/		
	
	add_action('application_top',  'compress::top');

	class compress {
	   public static function top(){
	      ob_start('compress_buffer');
	   }
	   
	   public static function buffer($buffer){
           $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);

           $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
           return $buffer;
	   }	   
	   
	}
	
	function compress_buffer($b){ 
	   return compress::buffer($b);
	}
?>