<?php
/*
Plugin Name: Галерея
Plugin URI: http://160.by/
Version: 1.2
Author: Евгений Фоминов
Author URI: http://160.by/
*/

add_action('main_page_admin', 'gal::page');
add_filter('product_info_assign',     'gal::info');

add_filter('news_product_add_tabs', 'gal::tabs');
add_action('insert_product', 'gal::insert');
add_action('remove_product', 'gal::remove_p');

class gal
{   
    public static function remove()
    {
        p::query("drop table gal;");
    }

    public static function tabs($arr)
    {
        $tab_content = '';
        $gal = '';
        include('gal.page.php');

        $arr['values'][] = array(
            'tab_name' => 'Галерея', 
            'tab_content' => $gal,
            'tab_id' =>'gal'
        );

        return $arr;
    }

    public static function insert($ar)
    {
        $products_id = $ar['products_id'];

        if ( isset($_FILES['gal']) && count($_FILES['gal']) > 0)
        {
            foreach ($_FILES['gal']['name'] as $m =>$v)
            {
                if ( !empty($v))
                {
                    move_uploaded_file($_FILES['gal']['tmp_name'][$m], DIR_FS_CATALOG.'images/gal/'.$v);
                    p::query('delete from gal where sort='.$m.' and products_id='.(int)$products_id);
                    p::query('insert into gal (products_id, sort,  image) values ("'.$products_id.'", "'.$m.'", "'.$v.'");');
                }
            }
        }

        if ( isset($_POST['gal_remove']))
        {

            foreach ($_POST['gal_remove'] as $m =>$v)
            {
                if ($v == 'on')
                {
                    $x = p::query('select * from gal where sort='.$m.' and products_id='.(int)$products_id);
                    $e = p::fetch_array($x);
                    
                    if (is_file(DIR_FS_CATALOG.'images/gal/'.$e['image']))
                    {
                        unlink(DIR_FS_CATALOG.'images/gal/'.$e['image']);
                    }
                    
                    p::query('delete from gal where sort='.$m.' and products_id='.(int)$products_id);
                }
            }
        }


    }


    public static function remove_p ($ar)
    {
        $products_id = $ar['products_id'];
        p::query('delete from gal where products_id='.(int)$products_id);
    }


    public static function install()
    {
        p::query("CREATE TABLE IF NOT EXISTS gal (
            gal_id int NOT NULL auto_increment,
            products_id int NOT NULL,
            sort int NOT NULL,
            image varchar(255) NOT NULL,
            PRIMARY KEY  (gal_id)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;");

        add_option('gal_status', 'true', 'radio', 'array("true", "false")');
    }

    public static function info($ar)
    {
        $products_id = $ar['var']['products_id'];
        
        $s = p::query('select * from gal where products_id='.$products_id);
        if ( p::num_rows($s) > 0)
        {
            $ll = array();
            while ($k = p::fetch_array($s))
            {
                $ll[] = '/images/gal/'.$k['image'];
            } 
            
            $ar['data']['gal'] = $ll;
        }
        
    
       return $ar;
    }

}



?>