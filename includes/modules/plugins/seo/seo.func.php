<?php
  	  function cleaner($name)
	  {
	      setlocale(LC_ALL, 'ru_RU.UTF-8');
          mb_internal_encoding('UTF-8');
		  $name = mb_strtolower($name);
	      $replace_param='/[^a-zA-Zа-яА-Я0-9]/';
     $cyrillic = array("ж", "ё", "й","ю", "ь","ч", "щ", "ц","у","к","е","н","г","ш", "з","х","ъ","ф","ы","в","а","п","р","о","л","д","э","я","с","м","и","т","б","Ё","Й","Ю","Ч","Ь","Щ","Ц","У","К","Е","Н","Г","Ш","З","Х","Ъ","Ф","Ы","В","А","П","Р","О","Л","Д","Ж","Э","Я","С","М","И","Т","Б");
     $translit = array("zh","yo","i","yu","","ch","sh","c","u","k","e","n","g","sh","z","h","",  "f",  "y",  "v",  "a",  "p",  "r",  "o",  "l",  "d",  "ye", "ya", "s",  "m",  "i",  "t",  "b",  "yo", "i",  "yu", "ch", "",  "sh", "c",  "u",  "k",  "e",  "n",  "g",  "sh", "z",  "h",  "",  "f",  "y",  "v",  "a",  "p",  "r",  "o",  "l",  "d",  "zh", "ye", "ya", "s",  "m",  "i",  "t",  "b");
     $name = str_replace($cyrillic, $translit, $name);    
     $name=preg_replace($replace_param,'-',$name);
     $name = urlencode($name);

	$name = preg_replace('~[/!;$,«»№":*^%#@\[\]&{}]+~s','-', $name);
	$name = preg_replace('~[--]+~s','-', $name);
	$name = trim($name);
	
	if (substr($name, strlen($name)-1)  == '-')
	{
		$name= substr($name, 0,strlen($name)-1);
	}
	
	
        return $name;
	  }
	  
?>