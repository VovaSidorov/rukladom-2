<?php
   add_action('page', 'seo::go');
   add_action('main_page_admin', 'seo::generator');
   add_action('insert_category', 'seo::insert_category');
   add_action('remove_categories', 'seo::remove_categories');
   add_action('insert_product', 'seo::insert_product');
   add_action('remove_product', 'seo::remove_product');
   add_action('insert_news', 'seo::insert_news');
   add_action('remove_news', 'seo::remove_news');
   
   class seo
   {	  
	  public static function generator()
	  {
		 echo '<h1>Генератор ЧПУ</h1>';
	  }		  
	 	  
	  public static function insert_category($ar)
	  {
		 $cat_id = (int)$ar['categories_id'];
		 $name = $ar['categories_name'][1];
		 $url = $ar['categories_url'];
		 
		 $al = seo::get_key($cat_id);
		 
		 if ( is_array($al) && count($al) > 0)
		 {
		    foreach ($al as $_id => $name)
			{
			   $al[$_id] = seo::clean( $name );
			}

			$url2 = implode('/', $al).'/';
		 }
		 
		 if (  empty( $url ) && !empty($name) ) $url = $url2;
		 
		 if (!empty ( $url ) )
		 {
		    p::query("update categories set categories_url='".$url."' where categories_id='".$cat_id."'"); 
		    p::query("replace into `seo` (`query`, `keyword`) VALUES ('index.php?cat=".$cat_id."', '".$url."');");
		 }
	  }		  
	 
	  // добавление чпу для новости
	  public static function insert_news($ar)
	  { 
		 $news_id = (int)$ar['news_id'];
		 $name = $ar['headline'];
		 $url = $ar['news_page_url'];
		 
		 if (  empty( $url ) && !empty($name) ) 
		 {
		    $n = seo::clean( $name );
		    $url = get_option('seo_news');
			$url = str_replace('%name%', $n, $url);
		 }
		 
		 if (!empty ( $url ) )
		 {
		    p::query("update latest_news set news_page_url='".$url."' where news_id='".$news_id."'"); 
		    p::query("replace into `seo` (`query`, `keyword`) VALUES ('news.php?news_id=".$news_id."', '".$url."');");
		 }
	  }
	  
	  public static function insert_product($ar)
	  {
	     $id = (int)$ar['products_id'];
		 $url = $ar['products_page_url'];
		 $name = $ar['products_name'][1];
		 
		 $cat_id = seo::cat_id($id);
		 $al = seo::get_key($cat_id);
		 $al[] = $name;
		 $url2 = '';
		 
         if ( is_array($al) && count($al) > 0)
		 {
		    foreach ($al as $_id => $name)
			{
			   $al[$_id] = seo::clean( $name );
			}

			$url2 = implode('/', $al).'/';
		 }
		 
		 if (  empty( $url ) && !empty($name) )  $url = $url2;
		 
         if ( !empty( $url ) ) 
		 {
		    p::query("update products set products_page_url='".$url."' where products_id='".$id."'"); 
		    p::query("replace INTO `seo` (`query`, `keyword`) VALUES ('product_info.php?products_id=".$id."', '".$url."');");
         }
	  }	 
	  
	  public static function cat_id($products_id)
	  {
	      $a = p::query('select * from products_to_categories where products_id='.(int)$products_id);
		  $b = p::fetch_array($a, true);
		  return $b['categories_id'];	  
	  }
	  
      public static function get_key($cat_id)
      {
	      $categories_query = "select c.categories_id,
                                           cd.categories_name,
                                           c.categories_image,
										    c.sort_order,
                                           c.parent_id from ".TABLE_CATEGORIES." c, ".TABLE_CATEGORIES_DESCRIPTION." cd
                                           where c.categories_status = '1'
                                           and c.categories_id = cd.categories_id
                                           order by c.sort_order, cd.categories_name";
								   
          $categories_query = p::query($categories_query);
		  $parent = array();
		  $cat = array();
		  $al = array();
		  while ($categories = p::fetch_array($categories_query, true)) 
          {
		     $parent[ $categories['parent_id'] ][ $categories['categories_id'] ] = array('name' => $categories['categories_name'] ); 
			 
			 $cat[ $categories['categories_id'] ] = array(
	         'name' => $categories['categories_name'], 
			 'parent'=> $categories['parent_id']
	);

		  }
		  
		  $par = $cat_id;
		  while ( $par != 0)
		  {
		      $al[] = $cat[ $par ]['name'];
		      $par = $cat[ $par ]['parent'];
			  
		  }
		  
		 return array_reverse ($al);  
	  }	  
	  
	  public static function remove_categories($ar)
	  {
	     $id = $ar['category_id'];
		 p::query("delete from seo where query='index.php?cat=".(int)$id."'");
	  }
	  	  
	  public static function remove_product($ar)
	  {
	     $id = $ar['products_id'];
		 p::query("delete from seo where query='product_info.php?products_id=".(int)$id."'");
	  }	  
	  	
	  public static function remove_news($ar)
	  {
	     $id = $ar['news_id'];
		 p::query("delete from seo where query='news.php?news_id=".(int)$id."'");
	  }
	  
	  public static function install()
	  {
	       p::query('CREATE TABLE IF NOT EXISTS `seo` (
  `seo_id` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `type` TINYINT(0) NOT NULL,
  PRIMARY KEY (`seo_id`),
  KEY `query` (`query`),
  KEY `keyword` (`keyword`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;;
            ');
			
		   add_option('seo_news', 'news/%name%');
	  }
	  
	  public static function remove()
	  {
	     //p::query('DROP TABLE seo;');
	  }
	  
	  public static function clean($name)
	  {
	         $replace_param='/[^a-zA-Zа-яА-Я0-9]/';
     $cyrillic = array("ж", "ё", "й","ю", "ь","ч", "щ", "ц","у","к","е","н","г","ш", "з","х","ъ","ф","ы","в","а","п","р","о","л","д","э","я","с","м","и","т","б","Ё","Й","Ю","Ч","Ь","Щ","Ц","У","К","Е","Н","Г","Ш","З","Х","Ъ","Ф","Ы","В","А","П","Р","О","Л","Д","Ж","Э","Я","С","М","И","Т","Б");
     $translit = array("zh","yo","i","yu","","ch","sh","c","u","k","e","n","g","sh","z","h","",  "f",  "y",  "v",  "a",  "p",  "r",  "o",  "l",  "d",  "ye", "ya", "s",  "m",  "i",  "t",  "b",  "yo", "i",  "yu", "ch", "",  "sh", "c",  "u",  "k",  "e",  "n",  "g",  "sh", "z",  "h",  "",  "f",  "y",  "v",  "a",  "p",  "r",  "o",  "l",  "d",  "zh", "ye", "ya", "s",  "m",  "i",  "t",  "b");
     $name = str_replace($cyrillic, $translit, $name);    
     $name=preg_replace($replace_param,'-',$name);
     $name = urlencode($name);

	$name = preg_replace('~[/!;$,«»№":*^%#@\[\]&{}]+~s','-', $name);
	$name = preg_replace('~[--]+~s','-', $name);
	$name = trim($name);	 
        return $name;
	  }
   }
?>