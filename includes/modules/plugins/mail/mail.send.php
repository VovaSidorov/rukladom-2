<?php 

$error = '';

function check_email_address($email) 
{
    // First, we check that there's one @ symbol, 
    // and that the lengths are right.
    if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
        // Email invalid because wrong number of characters 
        // in one section or wrong number of @ symbols.
        return false;
    }
    // Split it into sections to make life easier
    $email_array = explode("@", $email);
    $local_array = explode(".", $email_array[0]);
    for ($i = 0; $i < sizeof($local_array); $i++) {
        if
            (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&
                ↪'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$",
                $local_array[$i])) {
                return false;
        }
    }
    // Check if domain is IP. If not, 
    // it should be valid domain name
    if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
        $domain_array = explode(".", $email_array[1]);
        if (sizeof($domain_array) < 2) {
            return false; // Not enough parts to domain
        }
        for ($i = 0; $i < sizeof($domain_array); $i++) {
            if
                (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|
                    ↪([A-Za-z0-9]+))$",
                    $domain_array[$i])) {
                    return false;
            }
        }
    }
    return true;
}

if ( isset($_POST['mail']) and !empty($_POST['mail']) and check_email_address($_POST['mail']))
{
    $mail  = p::prepare_input($_POST['mail']);


    $b = p::query('select * from newsletter_recipients where customers_email_address="'.$mail.'"');

    if ( p::num_rows($b) > 0)
    {
        die('Вы уже подписаны на рассылку');
    }
    else
    {
        p::query('insert into newsletter_recipients (customers_email_address, customers_id, customers_status, customers_firstname, customers_lastname, mail_status, mail_key, date_added) values ("'.$mail.'", 0,0,"","",1,"","now()");');

        die('Вы успешно подписались на рассылку');
    }  
}
else
{
    die( '');
}


?>