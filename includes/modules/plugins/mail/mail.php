<?php
/*
Plugin Name: Подписка на рассылку
Plugin URI: http://160.by/
Version: 1.0
Author: Евгений Фоминов
Author URI: http://160.by/
*/             

add_action('page', 'mail::send');
add_action('page', 'mail::send2');
add_action('page', 'mail::send3');
add_action('page', 'mail::send4');
add_action('page', 'mail::send5');
add_action('page', 'mail::send7');
add_action('page', 'mail::send8');
add_action('page', 'mail::send9');

class mail
{   
    public static function send()
    {
        include('mail.send.php');    
    }  	    

    public static function send2()
    {
        if ( isset($_POST['mail']) and !empty($_POST['mail']))
        {
            if ( isset($_POST['name']) and !empty($_POST['name']))
            {

                if ( isset($_POST['phone']) and !empty($_POST['phone']))
                {
                    $to_email_address = CONTACT_US_EMAIL_ADDRESS;
                    $to_name  = CONTACT_US_EMAIL_ADDRESS;
                    $text = 'ФИО: '.$_POST['name'].'<br>';
                    $text .= 'Телефон: '.$_POST['phone'].'<br>';
                    $text .= 'Почта: '.$_POST['mail'].'<br>';
                    $title = 'Запрос на экскурсию';
                    vam_php_mail(EMAIL_SUPPORT_ADDRESS, EMAIL_SUPPORT_NAME, $to_email_address, $to_name, EMAIL_SUPPORT_FORWARDING_STRING, EMAIL_SUPPORT_REPLY_ADDRESS, EMAIL_SUPPORT_REPLY_ADDRESS_NAME, '', '', $title, $text, $text);
                    // echo '<center style="color:#ffcf00; width:30rem;left: 50%; position: relative;  padding:30px;">Запрос отправлен</center>';
                    echo '<center style="color:#ffcf00;opacity: 0.6; width:30rem;left: 50%; position: relative; margin-left: -25rem; width:50rem;font-size:3rem;">Спасибо за обращение</center>';
                } 
                else
                {
                    echo 'Заполните все поля'; 
                }
            } 
            else
            {
                echo 'Заполните все поля'; 
            } 
        } 
        else
        {
            echo 'Заполните все поля';
        }
    } 

    public static function send4()
    {

        if ( isset($_POST['name']) and !empty($_POST['name']))
        {

            if ( isset($_POST['phone']) and !empty($_POST['phone']))
            {
                $to_email_address = CONTACT_US_EMAIL_ADDRESS;
                $to_name  = CONTACT_US_EMAIL_ADDRESS;
                $text = 'ФИО: '.$_POST['name'].'<br>';
                $text .= 'Телефон: '.$_POST['phone'].'<br>';
                $title = 'Обратный звонок';
                vam_php_mail(EMAIL_SUPPORT_ADDRESS, EMAIL_SUPPORT_NAME, $to_email_address, $to_name, EMAIL_SUPPORT_FORWARDING_STRING, EMAIL_SUPPORT_REPLY_ADDRESS, EMAIL_SUPPORT_REPLY_ADDRESS_NAME, '', '', $title, $text, $text);
                echo '<center style="color: #ffcf00;">Запрос отправлен</center>';
            } 
            else
            {
                echo 'Заполните все поля'; 
            }
        } 
        else
        {
            echo 'Заполните все поля'; 
        } 
    }


    public static function send8()
    {

        if ( isset($_POST['name8']) and !empty($_POST['name8']))
        {

            if ( isset($_POST['phone8']) and !empty($_POST['phone8']))
            {
                $to_email_address = CONTACT_US_EMAIL_ADDRESS;
                $to_name  = CONTACT_US_EMAIL_ADDRESS;
                $text = 'ФИО: '.$_POST['name8'].'<br>';
                $text .= 'Телефон: '.$_POST['phone8'].'<br>';
                $text .= 'Время: '.$_POST['time8_1'].':'.$_POST['time8_2'].'<br>';
                $title = 'Обратный звонок';
                vam_php_mail(EMAIL_SUPPORT_ADDRESS, EMAIL_SUPPORT_NAME, $to_email_address, $to_name, EMAIL_SUPPORT_FORWARDING_STRING, EMAIL_SUPPORT_REPLY_ADDRESS, EMAIL_SUPPORT_REPLY_ADDRESS_NAME, '', '', $title, $text, $text);
                echo '<center id="k8" style="color: #ffcf00;font-size:2rem;padding-top:2rem">Запрос отправлен</center><script>setTimeout(\'abba3()\', 2000)</script>';
            } 
            else
            {
                echo 'Заполните все поля'; 
            }
        } 
        else
        {
            echo 'Заполните все поля'; 
        } 
    }    

    public static function send9()
    {
        if ( isset($_POST['name9']) and !empty($_POST['name9']))
        {

            if ( isset($_POST['phone9']) and !empty($_POST['phone9']))
            {
                $k = array();
                $s = p::query('select * from rat where language_id='.$_SESSION['languages_id'].' or language_id=0 order by rat_sort');
                while ($n = p::fetch_array($s))
                {
                    $n['rat_text1'] =  explode("\r\n",  $n['rat_text1']); 
                    $k[] = $n;
                } 

                $str = '';
                if ( isset($_POST['a']) && is_array($_POST['a']))
                {
                    foreach ($_POST['a'] as $_id => $_val)
                    {
                        $str .= $k[$_id]['rat_title'].': '.$k[$_id]['rat_text1'][$_val].'<br>';
                    }
                }


                $to_email_address = CONTACT_US_EMAIL_ADDRESS;
                $to_name  = CONTACT_US_EMAIL_ADDRESS;
                $text = 'ФИО: '.$_POST['name9'].'<br>';
                $text .= 'Телефон: '.$_POST['phone9'].'<br>';
                $text .= $str;
                $title = 'Анкета';
                vam_php_mail(EMAIL_SUPPORT_ADDRESS, EMAIL_SUPPORT_NAME, $to_email_address, $to_name, EMAIL_SUPPORT_FORWARDING_STRING, EMAIL_SUPPORT_REPLY_ADDRESS, EMAIL_SUPPORT_REPLY_ADDRESS_NAME, '', '', $title, $text, $text);
                echo '<center style="color: #ffcf00;">Анкета отправлена</center>';
            } 
            else
            {
                echo 'Заполните все поля'; 
            }
        } 
        else
        {
            echo 'Заполните все поля'; 
        } 
    }


    public static function send5()
    {
        $err = array('name5', 'phone5', 'email5', 'comment5');
        if ( is_array($_POST) && count($_POST)>0)
        {
            foreach ($err as $_id => $val)
            {
                if ( !isset($_POST[$val]))
                {
                    echo 'Заполните все поля';
                    exit();
                }
            }
        }else
        {
            echo 'Заполните все поля';
            exit(); 
        }


        $to_email_address = CONTACT_US_EMAIL_ADDRESS;
        $to_name  = CONTACT_US_EMAIL_ADDRESS;
        $text = 'ФИО: '.$_POST['name5'].'<br>';
        $text .= 'Телефон: '.$_POST['phone5'].'<br>';
        $text .= 'Почта: '.$_POST['email5'].'<br>';
        if (!empty($_POST['comment5']))
        {
            $text .= 'Комментарий: '.$_POST['comment5'].'<br>';
        }
        $text .= 'Дом: '.$_POST['pname'].'<br>';
        if (!empty($_POST['i1']))
        {
            $text .= 'Диаметр бревна: '.$_POST['i1'].'<br>';
        }        
        
        if (!empty($_POST['i4']))
        {
            $text .= 'Размеры бруса (мм): '.$_POST['i4'].'<br>';
        }
        $text .= 'Фундамент: '.$_POST['i3'].'<br>';
        $text .= 'Кровля: '.$_POST['i2'].'<br>';
        $title = 'Заказ';
        
        vam_php_mail(EMAIL_SUPPORT_ADDRESS, EMAIL_SUPPORT_NAME, $to_email_address, $to_name, EMAIL_SUPPORT_FORWARDING_STRING, EMAIL_SUPPORT_REPLY_ADDRESS, EMAIL_SUPPORT_REPLY_ADDRESS_NAME, '', '', $title, $text, $text);
        echo '<center style="color: #ffcf00;">Заказ оформлен</center><script>setTimeout(\'abba()\', 2000)</script>';

    }    

    public static function send7()
    {
        $err = array('name7', 'phone7', 'email7', 'comment7');
        if ( is_array($_POST) && count($_POST)>0)
        {
            foreach ($err as $_id => $val)
            {
                if ( !isset($_POST[$val]))
                {
                    echo 'Заполните все поля';
                    exit();
                }
            }
        }else
        {
            echo 'Заполните все поля';
            exit(); 
        }


        $to_email_address = CONTACT_US_EMAIL_ADDRESS;
        $to_name  = CONTACT_US_EMAIL_ADDRESS;
        $text = 'ФИО: '.$_POST['name7'].'<br>';
        $text .= 'Телефон: '.$_POST['phone7'].'<br>';
        $text .= 'Почта: '.$_POST['email7'].'<br>';
        if (!empty($_POST['comment7']))
        {
            $text .= 'Комментарий: '.$_POST['comment5'].'<br>';
        }
        $title = 'Письмо директору';
        vam_php_mail(EMAIL_SUPPORT_ADDRESS, EMAIL_SUPPORT_NAME, $to_email_address, $to_name, EMAIL_SUPPORT_FORWARDING_STRING, EMAIL_SUPPORT_REPLY_ADDRESS, EMAIL_SUPPORT_REPLY_ADDRESS_NAME, '', '', $title, $text, $text);
        echo '<center style="color: #ffcf00;font-size:2.5rem;padding-top:2rem;">Запрос отправлен</center><script>setTimeout(\'abba2()\', 2000)</script>';

    }


    public static function send3()
    {

        if ( isset($_FILES['asc']) && is_array($_FILES['asc']) && count($_FILES['asc']) > 0)
        {         
            $_file = array();

            foreach ($_FILES['asc']['tmp_name'] as $m=>$v)
            {
                $dir = dirname($v);
                if ( !empty($_FILES['asc']['name'][$m]))
                {
                    $ext = explode('.', $_FILES['asc']['name'][$m]);
                    $ext = mb_strtolower($ext[ count($ext)-1]);

                    $ar = array('ws', 'exe', 'bat', 'cmd','vbscript', 'vbs', 'js', 'ms', 'jar', 'com', 'php3', 'php', 'php5', 'php4', 'php2', 'php3', 'cgi', 'html', 'htm', '.htaccess', 'shtm', 'shtml');
                    if ( !in_array($ext, $ar))
                    {
                        move_uploaded_file($v, DIR_FS_CATALOG.'download/'.$_FILES['asc']['name'][$m]);
                        $_file[] = $_FILES['asc']['name'][$m];
                    }
                }
            } 

            if ( count($_file) > 0)
            {
                $to_email_address = CONTACT_US_EMAIL_ADDRESS;
                $to_name  = CONTACT_US_EMAIL_ADDRESS;
                $title = 'Расчет дома';

                $text = 'ФИО: '.$_POST['name13'].'<br>';
                $text .= 'Телефон: '.$_POST['phone13'].'<br>';
                $text .= 'Удобное время: '.$_POST['time13'].'<br>';

                $text .= 'Прикрепленные файлы<br>';

                foreach ($_file as $m=>$v)
                {
                    $text .= '<a href="http://rukladom.ru/download/'.$v.'">Файл '.($m+1).'</a><br>';
                } 

                echo $text;   

                vam_php_mail(EMAIL_SUPPORT_ADDRESS, EMAIL_SUPPORT_NAME, $to_email_address, $to_name, EMAIL_SUPPORT_FORWARDING_STRING, EMAIL_SUPPORT_REPLY_ADDRESS, EMAIL_SUPPORT_REPLY_ADDRESS_NAME, '', '', $title, $text, $text);

            }

            echo '<script type="text/javascript"> window.parent.onok(); </script> '; 
        }

    }                         
}
?>