<style>
    a.reder {color:red}
</style>
<?php
include(DIR_WS_CLASSES . 'language.php');
$lng = new language;


$languages_string = '';
$count_lng='';



reset($lng->catalog_languages); 

$am = array();
foreach ($lng->catalog_languages as $_n => $_v)
{
    $am[ $_v['id'] ] = $_n; 
} 

if ( isset($_GET['act']) && $_GET['act'] == 'update' && isset($_GET['id']))
{
    if ( isset($_FILES['lic_img']) and !empty($_FILES['lic_img']['name']))
    {
        $targetFile = DIR_FS_CATALOG.'images/lic/'. $_FILES['lic_img']['name'];
        $tempFile = $_FILES['lic_img']['tmp_name'];
        move_uploaded_file($tempFile, $targetFile);

        if ( is_file($targetFile))
        {
            $_POST['lic_img'] = $_FILES['lic_img']['name'];

        }
    } 
    else
    {
        if (!empty($_POST['lic_filename']))
        {
            $_POST['lic_img'] = $_POST['lic_filename'];
        }
    }	
    unset( $_POST['lic_filename'] );		
    p::perform('lic', p::prepare_input($_POST), 'update', 'lic_id="'.(int)$_GET['id'].'"');
}



if ( isset($_GET['act']) and $_GET['act'] == 'add')
{
   
    $lic_title = p::prepare_input($_POST['lic_title']);
    $lic_filename = p::prepare_input($_POST['lic_filename']);


    if ( isset($_FILES['lic_img']) and !empty($_FILES['lic_img']['name']))
    {
        $targetFile = DIR_FS_CATALOG.'images/lic/'. $_FILES['lic_img']['name'];
        $tempFile = $_FILES['lic_img']['tmp_name'];
        move_uploaded_file($tempFile, $targetFile);

        if ( is_file($targetFile))
        {
            $lic_filename = $_FILES['lic_img']['name'];

        }
    }  


    p::query('insert into lic ( lic_title, lic_img) values ( "'.$lic_title.'", "'.p::prepare_input($lic_filename).'");');
  

}

if ( isset($_GET['act']) and $_GET['act'] == 'remove' and isset($_GET['id']))
{ 
    $id= (int) $_GET['id'];
    p::query('delete from lic where lic_id='.$id);
}    

if ( isset($_GET['act']) and $_GET['act'] == 'up')
{
    if ( isset($_POST['sl_url']) and is_array($_POST['sl_url']))
    {
        foreach ($_POST['sl_url'] as $_id=>$_val)
        {
            p::query('update lic set lic_url="'.p::prepare_input($_val).'" where lic_id='.(int)$_id);
        }
    }

    if ( isset($_POST['sl_title']) and is_array($_POST['sl_title']))
    {
        foreach ($_POST['sl_title'] as $_id=>$_val)
        {
            p::query('update lic set lic_title="'.p::prepare_input($_val).'" where lic_id='.(int)$_id);
        }
    }        

}    

$s = p::query('select * from lic order by lic_group, lic_sort');

echo '<table width="100%"><tr><td width="*" valign="top">';
if ( p::num_rows($s) > 0)
{
    echo '<table width="100%" cellspacing="2" cellpadding="2" border="0" class="xr">';
    echo '<tr  class="dataTableHeadingRow" align="center">';
    echo '<td class="dataTableHeadingContent">Картинка</td>';
    echo '<td class="dataTableHeadingContent">Заголовок</td>';
    echo '<td class="dataTableHeadingContent">Действие</td>';
    echo '</tr>';


    $str_num = 0;
    $str_val = array();
    $i =0;

    $bs=array();

    while ( $n = p::fetch_array($s) )
    {
        $bs[ $n['lic_id'] ] =  $n; 
    }

    $start = 0;
    if ( isset($_GET['id']))
    {
        if ( isset(  $bs[  $_GET['id']  ]  ))
        {
            $start =  $_GET['id'];
        }
        else
        {
            $start =current( reset($bs) );
        }
    }
    else
    {
        $start =current( reset($bs) );
    }

    foreach ($bs as $m => $n)
    {
        if ( $start == $m )
        {
            echo '<tr align="center" class="dataTableRowSelected" style="background:#FFDAB9">' . "\n";
        }
        else
        {   
            echo '<tr align="center" onclick="document.location.href=\'plugins.php?main_page=lic::page&id='.$n['lic_id'].'\'" onmouseover="this.style.background=\'#e9fff1\';" onmouseout="this.style.background=\''.'#f9f9ff'.'\';"  style="background-color:#f9f9ff';
            echo ';" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'">' . "\n";
        }  

        $_img = '';
        if (!empty($n['lic_img'])) $_img = '<img width="100px" src="/images/lic/'.$n['lic_img'].'">'; else $_img = 'нет';
        //if (!empty($n['lic_img'])) $_img = 'да'; else $_img = 'нет';

        echo '<td>'.$_img.'</td>';

        echo '<td>'.$n['lic_title'].'</td>';
        echo '<td><a class="reder" href="plugins.php?main_page=lic::page&act=remove&id='.$n['lic_id'].'" onclick="return confirm(\'Действительно хотите удалить отзыв?\')">Удалить</a></td>';
        echo '</tr>';
    }
    echo '</table>';
}
else
{
    echo 'нет добавленных отзывов';
}

echo '</td><td width="350px" valign="top">';

if ( is_array($bs) && count($bs) > 0 && isset($bs[$start]) )
{


    ?>

    <table class="headTable" border="0" cellpadding="2" cellspacing="0" width="97%" align="center">
        <tr>
            <td><b>Редактировать</b></td>
        </tr>
        </tbody></table>
    <?php   

    $data = $bs[$start];


echo '<form action="plugins.php?main_page=lic::page&act=update&id='.$data['lic_id'].'" method="post" enctype="multipart/form-data">';
echo '<table class="contentTable" border="0" cellpadding="2" cellspacing="0" width="97%" align="center">';
echo '<tr><td>Заголовок</td><td><input type="text" style="width:100%" name="lic_title" value="'.$data['lic_title'].'"></td></tr>';
echo '<tr><td>Картинка</td><td><input type="file" name="lic_img"></td></tr>';
echo '<tr><td>Картинка</td><td><input type="text" style="width:100%" name="lic_filename" value="'.$data['lic_img'].'"></td></tr>';
echo '<tr><td></td><td align="right"><button>Сохранить</button></td></tr>';
echo '<table>';
echo '</form><br>';
}

?>
<table class="headTable" border="0" cellpadding="2" cellspacing="0" width="97%" align="center">
    <tr>
        <td><b>Добавить</b></td>
    </tr>
    </tbody></table>
<?php   
$st = '<select name="language_id">';

foreach ($am as $_id1 => $_val1)
 {
    $st .= '<option value="'.$_id1.'">'.$_val1.'</option>';   
} 


$st .='</select>';

echo '<form action="plugins.php?main_page=lic::page&act=add" method="post" enctype="multipart/form-data">';
echo '<table class="contentTable" border="0" cellpadding="2" cellspacing="0" width="97%" align="center">';
echo '<tr><td>Заголовок</td><td><input type="text" style="width:100%" name="lic_title"></td></tr>';
echo '<tr><td>Картинка</td><td><input type="file" name="lic_img"></td></tr>';
echo '<tr><td>Картинка</td><td><input type="text" style="width:100%" name="lic_filename"></td></tr>';
echo '<tr><td></td><td align="right"><button>Добавить</button></td></tr>';
echo '<table>';
echo '</form>';

echo '</td></tr></table>';

