<?php
/* -----------------------------------------------------------------------------------------
   $Id: news.php 1292 2007-02-06 20:41:56 VaM $   

   VaM Shop - open source ecommerce solution
   http://vamshop.ru
   http://vamshop.com

   Copyright (c) 2007 VaM Shop
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(new_products.php,v 1.33 2003/02/12); www.oscommerce.com
   (c) 2003	 nextcommerce (new_products.php,v 1.9 2003/08/17); www.nextcommerce.org
   (c) 2004	 xt:Commerce (new_products.php,v 1.9 2003/08/17); www.xt-commerce.com

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

$module = new vamTemplate;
$module->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');
    
    $reviews_query = vam_db_query("select reviews_id, customers_name, date_added, reviews_text, image, language_id from reviews_description_main where language_id=".$_SESSION['languages_id']." order by date_added desc");
	$rew = array();
	
	include(DIR_FS_INC . 'vam_date_long.inc.php');
    while ($reviews = vam_db_fetch_array($reviews_query))
	{
	   $m = $reviews;
	   $m['customers_name'] = str_replace(' ', '<br>', $m['customers_name']);
	   
	   $dt =  vam_date_long($m['date_added']);
	   $date_elements  = explode(" ", $dt);
	   unset($date_elements[0]);
	   $m['date_added'] = implode(" ", $date_elements);

       if (empty($m['image'])) $m['image'] = 'autor-no.jpg';
	   $rew[] =  $m;
	}

	$module->assign('module_content',$rew);
	
	// set cache ID
	 if (!CacheCheck()) {
		$module->caching = 0;
        $module= $module->fetch(CURRENT_TEMPLATE.'/module/reviews_main.html');
	} else {
        $module->caching = 1;
        $module->cache_lifetime=CACHE_LIFETIME;
        $module->cache_modified_check=CACHE_CHECK;
        $module = $module->fetch(CURRENT_TEMPLATE.'/module/reviews_main.html',$cache_id);
	}
	$default->assign('MODULE_reviews', $module);

?>