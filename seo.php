<?php
/*160.by */
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
require_once ('includes/configure.php');
require_once (DIR_FS_INC.'vam_mb_utf8.inc.php');
require_once (DIR_FS_INC.'vam_db_connect.inc.php');
require_once (DIR_FS_INC.'vam_db_close.inc.php');
require_once (DIR_FS_INC.'vam_db_error.inc.php');
require_once (DIR_FS_INC.'vam_db_perform.inc.php');
require_once (DIR_FS_INC.'vam_db_query.inc.php');
require_once (DIR_FS_INC.'vam_db_queryCached.inc.php');
require_once (DIR_FS_INC.'vam_db_fetch_array.inc.php');
require_once (DIR_FS_INC.'vam_db_num_rows.inc.php');
require_once (DIR_FS_INC.'vam_db_data_seek.inc.php');
require_once (DIR_FS_INC.'vam_db_insert_id.inc.php');
require_once (DIR_FS_INC.'vam_db_free_result.inc.php');
require_once (DIR_FS_INC.'vam_db_fetch_fields.inc.php');
require_once (DIR_FS_INC.'vam_db_output.inc.php');
require_once (DIR_FS_INC.'vam_db_input.inc.php');
require_once (DIR_FS_INC.'vam_db_prepare_input.inc.php');

vam_db_connect() or die('Unable to connect to database server!');

function request_URI() {
    if(!isset($_SERVER['REQUEST_URI'])) {
        $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'];
        if($_SERVER['QUERY_STRING']) {
            $_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
        }
    }
    return $_SERVER['REQUEST_URI'];
}


if ( isset($_GET['u']) && !empty( $_GET['u'] ) ){ 
   $uri = request_URI();

   $uri = explode('?', $uri);

   $uri_t = '';
   if ( is_array($uri) && count($uri) ==2)
   {
      $uri_t = '&'.$uri[1];
   }
   
   $a = vam_db_query('select * from seo where keyword="'.$_GET['u'].'" limit 1');
   
   if ( vam_db_num_rows($a) ==0 )
   {
      $f = explode('.', $_GET['u']);
	  
	  if ( $f[count($f)-1] == 'html' )
	  {
	       include_once('manager.php');
	  }
	  else
	  {
	      //header('HTTP/1.1 404 Not Found');
         // header('Status: 404 Not Found');
		  include('404.php');
	  }
      exit();
   }
   
   $b = vam_db_fetch_array($a);

   if ($b['type'] == 301)
   {
      header("Location: ".HTTP_SERVER.'/'.$b['query'],TRUE,301);
	  exit();
   }
   
   if ($b['type'] == 302)
   {
      header("Location: ".HTTP_SERVER.'/'.$b['query'],TRUE,302);
	  exit();
   }
   
   $query = $b['query'];
   $query = explode('?', $query);
    

   if (isset($query[1])){
      $query[1] .=$uri_t; 

      $sr = explode('&', $query[1]);

	  if ( is_array($sr) && count($sr) > 0){
	    foreach ($sr as $val){
		   $sm = explode('=', $val);
		   if ( count($sm) == 2) $_GET[ $sm[0] ] = $sm[1];
		}
	  }
	  unset($_GET['u']);
	  $PHP_SELF = '/'.$query[0];
     /* print_r($_GET);
      echo $query[0];
      die();
	  */
      
      include($query[0]);
   }
   
}
elseif (isset($_GET['u']) && empty($_GET['u']) )
{
   $PHP_SELF = '/index.php';
   include('index.php');
}
else
{
   // header('HTTP/1.1 404 Not Found');
   // header('Status: 404 Not Found');
	$PHP_SELF = '/404.php';
	include('404.php');
}
?>