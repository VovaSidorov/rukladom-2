<?php /* Smarty version 2.6.25, created on 2015-11-17 19:15:35
         compiled from dom/module/new_products_default.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'dom/module/new_products_default.html', 1, false),array('modifier', 'round', 'dom/module/new_products_default.html', 11, false),array('modifier', 'strip_tags', 'dom/module/new_products_default.html', 16, false),array('modifier', 'vam_truncate', 'dom/module/new_products_default.html', 16, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => ($this->_tpl_vars['language'])."/lang_".($this->_tpl_vars['language']).".conf",'section' => 'new_products'), $this);?>
 <h1><a href="<?php echo $this->_tpl_vars['NEW_PRODUCTS_LINK']; ?>
"><?php echo $this->_config[0]['vars']['heading_text']; ?>
</a></h1>
<div class="page">
<div class="pageItem">

<!-- start: products listing -->
<div class="row-fluid shop-products">
	<ul class="thumbnails">
		<?php $_from = $this->_tpl_vars['module_content']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['aussen'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['aussen']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['module_data']):
        $this->_foreach['aussen']['iteration']++;
?>
		<li class="item span4<?php if (($this->_foreach['aussen']['iteration']-1) % 3 == 0): ?> first<?php endif; ?>">
			<div class="thumbnail text-center">
				<?php if ($this->_tpl_vars['module_data']['PRODUCTS_SPECIAL'] > 0): ?><div class="description"><span class="discount">-<?php echo ((is_array($_tmp=$this->_tpl_vars['module_data']['PRODUCTS_SPECIAL'])) ? $this->_run_mod_handler('round', true, $_tmp) : round($_tmp)); ?>
%</span></div><?php endif; ?>
				<a href="<?php echo $this->_tpl_vars['module_data']['PRODUCTS_LINK']; ?>
" class="image"><img src="<?php echo $this->_tpl_vars['module_data']['PRODUCTS_IMAGE']; ?>
" alt="<?php echo $this->_tpl_vars['module_data']['PRODUCTS_NAME']; ?>
" /><span class="frame-overlay"></span><span class="price"><?php echo $this->_tpl_vars['module_data']['PRODUCTS_PRICE']; ?>
</span><?php if ($this->_tpl_vars['module_data']['PRODUCTS_LABEL']): ?><?php echo $this->_tpl_vars['module_data']['PRODUCTS_LABEL']; ?>
<?php endif; ?></a>
			<div class="inner notop nobottom text-left">
				<h4 class="title"><a href="<?php echo $this->_tpl_vars['module_data']['PRODUCTS_LINK']; ?>
"><?php echo $this->_tpl_vars['module_data']['PRODUCTS_NAME']; ?>
</a></h4>
				<?php if ($this->_tpl_vars['module_data']['REVIEWS_TOTAL'] > 0): ?><div class="description"><span class="rating"><?php echo $this->_tpl_vars['module_data']['REVIEWS_STAR_RATING']; ?>
</span> <span class="reviews"><?php echo @TEXT_TOTAL_REVIEWS; ?>
: <?php echo $this->_tpl_vars['module_data']['REVIEWS_TOTAL']; ?>
</span></div><?php endif; ?>
				<div class="description"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['module_data']['PRODUCTS_SHORT_DESCRIPTION'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('vam_truncate', true, $_tmp, 30, "...", true) : smarty_modifier_vam_truncate($_tmp, 30, "...", true)); ?>
</div>
			</div>
			</div>
			<div class="inner darken notop">
				<?php echo $this->_tpl_vars['module_data']['PRODUCTS_BUTTON_BUY_NOW_NEW']; ?>

			</div>
		</li>
		<?php endforeach; endif; unset($_from); ?>
	</ul>
</div>  
<!-- end: products listing -->  
  
<div class="clear"></div>
</div>

</div>