<?php /* Smarty version 2.6.25, created on 2015-11-17 19:15:35
         compiled from dom/module/latest_news_default.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'dom/module/latest_news_default.html', 1, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => ($this->_tpl_vars['language'])."/lang_".($this->_tpl_vars['language']).".conf",'section' => 'latest_news'), $this);?>

<?php echo smarty_function_config_load(array('file' => ($this->_tpl_vars['language'])."/dom.conf",'section' => 'main'), $this);?>

      <section class="news">
        <div class="container">
          <div class="row"><a href="<?php echo $this->_tpl_vars['NEWS_LINK']; ?>
" class="link-all-news"><?php echo $this->_config[0]['vars']['head1']; ?>
</a>
            <div id="newsSlider" class="owl-carousel owl-theme">
               <?php $_from = $this->_tpl_vars['module_content']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['aussen'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['aussen']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['module_data']):
        $this->_foreach['aussen']['iteration']++;
?>
              <div class="content"><span class="date-news"><?php echo $this->_tpl_vars['module_data']['NEWS_DATA']; ?>
</span>
                <div class="clearfix"></div><a href="<?php echo $this->_tpl_vars['module_data']['NEWS_LINK_MORE']; ?>
" class="link-news"><?php echo $this->_tpl_vars['module_data']['NEWS_HEADING']; ?>
</a>
              </div>
              <?php endforeach; endif; unset($_from); ?>
            </div>
          </div>
        </div>
       </section>