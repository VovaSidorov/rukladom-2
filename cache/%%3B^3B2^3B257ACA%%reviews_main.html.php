<?php /* Smarty version 2.6.25, created on 2015-11-17 19:15:35
         compiled from dom/module/reviews_main.html */ ?>
     <section class="comments">
        <div class="container">
          <div class="row">
            <h3 class="title">Отзывы клиентов</h3>
            <div id="reviewSlider" class="owl-carousel owl-theme">
              <?php $_from = $this->_tpl_vars['module_content']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['aussen'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['aussen']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['module_data']):
        $this->_foreach['aussen']['iteration']++;
?>
              <div class="content">
                <div class="row">
                  <div class="col-xs-2 col-sm-2 col-md-2"><img src="/images/reviews_main/<?php echo $this->_tpl_vars['module_data']['image']; ?>
" alt="Autor" class="autor-img resp"></div>
                  <div class="col-xs-10 col-sm-10 col-md-10">
                    <h5 class="autor-name"><?php echo $this->_tpl_vars['module_data']['customers_name']; ?>
</h5>
                    <hr>
                    <p class="comment"><?php echo $this->_tpl_vars['module_data']['reviews_text']; ?>
</p>
                  </div>
                </div>
              </div>
              <?php endforeach; endif; unset($_from); ?>

            </div>
          </div>
        </div>
      </section>