<?php /* Smarty version 2.6.25, created on 2015-11-17 19:15:32
         compiled from dom/index.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'dom/index.html', 1, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => ($this->_tpl_vars['language'])."/lang_".($this->_tpl_vars['language']).".conf",'section' => 'index'), $this);?>

<?php echo smarty_function_config_load(array('file' => ($this->_tpl_vars['language'])."/lang_".($this->_tpl_vars['language']).".conf",'section' => 'boxes'), $this);?>

<?php echo smarty_function_config_load(array('file' => ($this->_tpl_vars['language'])."/dom.conf",'section' => 'main'), $this);?>

<noscript>Включите Java Script</noscript>
<div class="clearfix"></div>
<!--[if lt IE 9]>
<p class="browsehappy">Вы используете <strong>неподдерживаемый</strong> браузер. Пожалуйста, скачайте новый браузер <a href="http://browsehappy.com/"><strong>здесь</strong></a></p>
<![endif]-->
      <style>
        <?php echo ' .hi {display:none}    '; ?>

      </style>


  
 
<?php if ($this->_tpl_vars['mainer'] == 1): ?>
<div class="prev-slide">  <?php endif; ?>
    <header>
        <div class="header-nav-mobile am hi">
            <ul>
                <li><a href="/catalog.html" class="active"><?php echo $this->_config[0]['vars']['menu1']; ?>
</a></li>
                <li><a href="/company.html"><?php echo $this->_config[0]['vars']['menu2']; ?>
</a></li>
                <li><a href="/faq.php"><?php echo $this->_config[0]['vars']['menu4']; ?>
</a></li>
                <li><a href="/articles.php"><?php echo $this->_config[0]['vars']['menu5']; ?>
</a></li>
            </ul>
        </div>
        <div class="top-header"><a href="/" class="logo"><i class="icon-logo"></i></a></div>
        <div class="middle-header"><a href="#" class="icon-menu"><i class="icon-line"></i><i class="icon-line"></i><i class="icon-line"></i></a>
            <nav class="header-nav">
                <ul>    <?php global $PHP_SELF; ?>
                    <li><a href="/catalog.html"<?php if ($_GET['cat']==1) echo ' class="active"'; ?>><?php echo $this->_config[0]['vars']['menu1']; ?>
</a></li>
                    <li><a href="/company.html"<?php if ( isset($_GET['coID']) && $_GET['coID'] == 4 ) echo ' class="active"'; ?>><?php echo $this->_config[0]['vars']['menu2']; ?>
</a></li>
                    <li><a href="/faq.php"<?php if ( strstr($PHP_SELF, 'faq.php') ) echo ' class="active"'; ?>><?php echo $this->_config[0]['vars']['menu4']; ?>
</a></li>
                    <li><a href="/articles.php"<?php if ( strstr($PHP_SELF, 'articles.php') ) echo ' class="active"'; ?>><?php echo $this->_config[0]['vars']['menu5']; ?>
</a></li>
                </ul>
            </nav>
            <hr>
            <?php echo $this->_tpl_vars['box_LANGUAGES']; ?>


        </div>
        <div class="bottom-header"><a href="contacts.html" class="contact"><i class="icon-contact"></i><?php echo $this->_config[0]['vars']['menu6']; ?>
</a>
            <div class="block-phone">
                <p class="phone" url="tel:<?php echo @STORE_TELEPHONE3_PLAIN; ?>
"><span><?php echo @STORE_TELEPHONE3; ?>
</span></p> 
                <div class="clearfix"></div>
                <?php if ($_SESSION['customers_status']['customers_status_id'] == 0){ ?> <a style="color:red; border-bottom: 1px dashed red;" class="kl" href="/admin/start.php">Админка</a> <?php }else {  ?><a href="#" class="order-phone"><?php echo $this->_config[0]['vars']['menu7']; ?>
</a><?php } ?>
            </div>
        </div>
        <?php if ($this->_tpl_vars['mainer'] == 1): ?>
        <div class="clearfix"></div>
        <div class="container">
            <div class="row">
                <h1 class="header-title"><?php echo $this->_config[0]['vars']['con14']; ?>
</h1>
                <hr class="sub-line">
                <p class="sub-title"><?php echo $this->_config[0]['vars']['con15']; ?>
</p>
                <div class="block-options">
                
                <input type="hidden" id="min" value="<?php echo $this->_tpl_vars['min']; ?>
">
                <input type="hidden" id="max" value="<?php echo $this->_tpl_vars['max']; ?>
">
                
                    <form id="filter" method="get" action="products_filter.php" name="filter">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="block-storeys">
                                    <h5 class="option-title"><?php echo $this->_config[0]['vars']['con16']; ?>
</h5>
                                    <div class="row">
                                        <div class="col-xs-2 col-sm-2 col-md-2"><a href="#" v="1" class="option-link f16" v2="1">1</a></div>
                                        <div class="col-xs-6 col-sm-6 col-md-6"><a href="#" v="2" class="option-link f16" v2="<?php echo $this->_config[0]['vars']['con17']; ?>
"><?php echo $this->_config[0]['vars']['con17']; ?>
</a></div>
                                        <div class="col-xs-2 col-sm-2 col-md-2"><a href="#" v="3" class="option-link f16" v2="2">2</a></div>
                                        <div class="col-xs-2 col-sm-2 col-md-2" ><a  href="#" v="4"  class="option-link2 option-link1 f16" v2="<?php echo $this->_config[0]['vars']['f26']; ?>
"><?php echo $this->_config[0]['vars']['f26']; ?>
</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-8 col-sm-4 col-md-4">
                                <h5 class="option-title"><?php echo $this->_config[0]['vars']['con18']; ?>
</h5>
                                <div class="row">
                                <input type="hidden" name="f21" value="">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <input type="text" id="f21_1" placeholder="<?php echo $this->_config[0]['vars']['con19']; ?>
" class="from">
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <input type="text" id="f21_2" placeholder="<?php echo $this->_config[0]['vars']['con20']; ?>
" class="to">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-2 col-md-2">
                                <h5 class="option-title"><?php echo $this->_config[0]['vars']['con21']; ?>
</h5>
                                <select name="f18" class="tenants">
                                   <option value="2" selected="selected">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6 и более</option>
                                </select>
                            </div>
                            <div class="block-price">
                                <div class="row">
                                   <input type="hidden" name="min" value="">
                                   <input type="hidden" name="max" value="">
                                    <div class="col-xs-12 col-sm-6 col-sm-push-3 col-md-6 col-md-push-3">
                                        <input id="range-slider" type="text" value="">
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-sm-pull-6 col-md-3 col-md-pull-6">
                                        <div class="block-input">
                                            <input id="range-from" type="text"><span class="span-from"><?php echo $this->_config[0]['vars']['con19']; ?>
</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-md-3">
                                        <div class="block-input">
                                            <input id="range-to" type="text"><span class="span-to"><?php echo $this->_config[0]['vars']['con20']; ?>
</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <input type="submit" value="<?php echo $this->_config[0]['vars']['con22']; ?>
" class="submit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </header>
    <?php if ($this->_tpl_vars['mainer'] == 1): ?> </div> <?php endif; ?>
<main>
<section class="am1"><a href="#" class="hide-order"><i class="icon-cross"></i></a>
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
              <div class="modal">
                <h2 class="title-modal"><?php echo $this->_config[0]['vars']['menu7']; ?>
</h2>
                <form name="form8">
                  <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                      <label for="name8"><?php echo $this->_config[0]['vars']['text14']; ?>
</label>
                      <input id="name8" type="text" name="name8">
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                      <label for="phone8"><?php echo $this->_config[0]['vars']['head_mail_t6']; ?>
</label>
                      <input id="phone8" type="text" name="phone8">
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                      <label for="time8"><?php echo $this->_config[0]['vars']['f15']; ?>
&nbsp;</label>
                      <div class="hours_call_select" style="display: none;">
                                        <span>20</span>
                                        <span>19</span>                  
                                        <span>18</span>
                                        <span>17</span>
                                        <span>16</span>
                                        <span>15</span>
                                        <span>14</span>
                                        <span>13</span>
                                        <span>12</span>
                                        <span>11</span>
                                        <span>10</span>
                                        <span>09</span>
                                </div>
                      <input id="time8_1" type="text" value="00" readonly name="time8_1"><span style="color:#fff; float:left; padding:1rem;">:</span>
                             <div class="minutes_call_select" style="display:none">
                                        <span>50</span>
                                        <span>40</span>
                                        <span>30</span>
                                        <span>20</span>
                                        <span>10</span>
                                        <span>00</span>                    
                                 </div> 
                      <input id="time8_2" type="text" readonly value="00" name="time8_2">
              
                    </div>
                  </div>
                  <div class="inner-hr">
                    <hr>
                  </div>
                  <button class="order"><?php echo $this->_config[0]['vars']['file_button']; ?>
</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
      
<section class="am2"><a href="#" class="hide-order"><i class="icon-cross"></i></a>
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
              <div class="modal">
                <h2 class="title-modal"><?php echo $this->_config[0]['vars']['f17']; ?>
</h2>
                <form name="form12">
                  <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                      <label for="name12"><?php echo $this->_config[0]['vars']['text14']; ?>
</label>
                      <input id="name12" type="text" name="name12">
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                      <label for="phone12"><?php echo $this->_config[0]['vars']['head_mail_t6']; ?>
</label>
                      <input id="phone12" type="text" name="phone12">
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                      <label for="time12"><?php echo $this->_config[0]['vars']['f15']; ?>
&nbsp;</label>
                      <div class="hours_call_select2" style="display: none;">
                                        <span>20</span>
                                        <span>19</span>                  
                                        <span>18</span>
                                        <span>17</span>
                                        <span>16</span>
                                        <span>15</span>
                                        <span>14</span>
                                        <span>13</span>
                                        <span>12</span>
                                        <span>11</span>
                                        <span>10</span>
                                        <span>09</span>
                                </div>
                      <input id="time12_1" type="text" value="00" readonly name="time12_1"><span style="color:#fff; float:left; padding:1rem;">:</span>
                             <div class="minutes_call_select2" style="display:none">
                                        <span>50</span>
                                        <span>40</span>
                                        <span>30</span>
                                        <span>20</span>
                                        <span>10</span>
                                        <span>00</span>                    
                                 </div> 
                      <input id="time12_2" type="text" readonly value="00" name="time12_2">
              
                    </div>
                  </div>
                  <div class="inner-hr">
                    <hr>
                  </div>
                  <button class="order"><?php echo $this->_config[0]['vars']['file_button']; ?>
</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>      
    <?php echo $this->_tpl_vars['main_content']; ?>

</main>
<footer>
    <div class="container">
        <div class="row">
            <nav class="nav-footer">
                <ul>
                    <li><a href="/catalog.html" class="active"><?php echo $this->_config[0]['vars']['menu1']; ?>
</a></li>
                    <li><a href="/company.html"><?php echo $this->_config[0]['vars']['menu2']; ?>
</a></li>
                    <li><a href="/faq.php"><?php echo $this->_config[0]['vars']['menu4']; ?>
</a></li>
                    <li><a href="/articles.php"><?php echo $this->_config[0]['vars']['menu5']; ?>
</a></li>
                </ul>
            </nav>
            <div class="inner-hr">
                <hr>
            </div>
            <div class="block-phone">
                <p class="phone" url="tel:<?php echo @STORE_TELEPHONE3_PLAIN; ?>
"><span><?php echo @STORE_TELEPHONE3; ?>
</span></p>
                <div class="clearfix"></div><a href="#" class="order-phone"><?php echo $this->_config[0]['vars']['menu7']; ?>
</a>
                <br><span class="pip"><?php echo $this->_config[0]['vars']['f28']; ?>
</span>
            </div>
            <div class="footer-bottom">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-sm-push-4">
                        <div class="block-social"><a href="#" class="link-vk"></a><a href="#" class="link-facebook"></a><a href="#" class="link-inst"></a></div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-sm-pull-4">
                        <p class="copyright"><i class="icon-copyright"></i>Copyright 2003-2015 ГК <span>«РУКЛАД»</span></p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <p class="development"><?php echo $this->_config[0]['vars']['con24']; ?>
 — <a href="http://nexxstudio.ru/">NEXX<span style="color:#ffffff">STUDIO</span><b>.</b></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript" src="/templates/dom/tpl/js/jquery/dist/jquery.min.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/modernizr/modernizr.min.js"></script>
   
<script type="text/javascript" src="/templates/dom/tpl/js/respond/dest/respond.src.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/REM-unit-polyfill/js/rem.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/jquery-selectBox/jquery.selectBox.min.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/ion.rangeslider/js/ion.rangeSlider.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/flipclock/compiled/flipclock.min.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/owl.carousel/dist/owl.carousel.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/easytabs/lib/jquery.easytabs.min.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/jquery.scrollTo/jquery.scrollTo.min.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/jquery-validation/dist/jquery.validate.min.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/waypoints/lib/jquery.waypoints.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/fancybox/source/jquery.fancybox.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/jquery.arcticmodal.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/js/jquery.slides.min.js"></script>  



<?php 
    global $PHP_SELF;
    
     echo "<script>";
     echo "kr='';"; 
     echo "run='';"; 
        
    if ( strstr($PHP_SELF, 'products_filter.php') && isset($_GET['max']) )
    {
        echo "kr='1';";
    }
    
     echo "</script>";
    
    if ( strstr($PHP_SELF, '404.php') )
    {
         echo "<script>";
        echo "run='';"; 
         echo "</script>";
    }
    elseif ( strstr($PHP_SELF, 'product_info.php') )
    {
         echo "<script>";
        echo "run='/templates/dom/tpl/card.min.js';"; 
        echo "</script>";
         ?><script type="text/javascript" src="/templates/dom/tpl/card.min.js"></script> <?php  
    }   
    elseif ( strstr($PHP_SELF, 'faq.php') )
    {
        echo "<script>";
        echo "run='/templates/dom/tpl/questions.min.js';"; 
        echo "</script>";
         ?><script type="text/javascript" src="/templates/dom/tpl/questions.min.js"></script> <?php  
    }
    elseif ( isset($_GET['cat']) || strstr($PHP_SELF, 'products_filter.php') )
    {
         echo "<script>";
        echo "run='/templates/dom/tpl/catalog.min.js';"; 
        echo "</script>";
         ?><script type="text/javascript" src="/templates/dom/tpl/catalog.min.js"></script> <?php  
    }        
    elseif ( strstr($PHP_SELF, 'shop_content.php') && isset($_GET['coID'])  && $_GET['coID'] == 15)
    {
         echo "<script>";
        echo "run='/templates/dom/tpl/contacts.min.js';"; 
        echo "</script>";
         ?><script type="text/javascript" src="/templates/dom/tpl/contacts.min.js"></script> <?php  
    }      
    
    elseif ( strstr($PHP_SELF, 'shop_content.php') && isset($_GET['coID']))
    {
         echo "<script>";
        echo "run='/templates/dom/tpl/company.min.js';"; 
        echo "</script>";
         ?><script type="text/javascript" src="/templates/dom/tpl/company.min.js"></script> <?php  
    }    
    elseif ( strstr($PHP_SELF, 'shop_content.php') || strstr($PHP_SELF, 'rat.php') || strstr($PHP_SELF, 'news.php') || strstr($PHP_SELF, 'article_info.php' )|| strstr($PHP_SELF, 'cookie_usage.php') || strstr($PHP_SELF, 'checkout_success.php') || strstr($PHP_SELF, 'password_double_opt.php') || strstr($PHP_SELF, 'logoff.php')  || strstr($PHP_SELF, 'login.php') )
    {
         echo "<script>";
        echo "run='/templates/dom/tpl/company.min.js';"; 
         echo "</script>";
         ?><script type="text/javascript" src="/templates/dom/tpl/company.min.js"></script> <?php  
    } 
    elseif ( strstr($PHP_SELF, 'articles.php') )
    {
         echo "<script>";
        echo "run='/templates/dom/tpl/articles.min.js';"; 
        echo "</script>";
         ?><script type="text/javascript" src="/templates/dom/tpl/articles.min.js"></script> <?php  
    }
    else
    {
        echo "<script>";
        echo "run='/templates/dom/tpl/main.min.js';";
        echo "</script>";
         ?><script type="text/javascript" src="/templates/dom/tpl/main.min.js"></script> <?php  
    }
     ?>
<script src="/templates/dom/tpl/mask.js"></script>    
<script type="text/javascript" src="/templates/dom/tpl/m.js"></script> 
<script type="text/javascript" src="/templates/dom/tpl/loader.js"></script>  
<!--SCB--><script type="text/javascript" async src="http://smartcallback.ru/api/SmartCallBack.js?t=czOYbSb9TagY9XqOSQE3" charset="utf-8"></script><!--END SCB-->    