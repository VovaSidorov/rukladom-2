<?php /* Smarty version 2.6.25, created on 2015-11-17 19:15:35
         compiled from dom/module/main_content.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'dom/module/main_content.html', 1, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => ($this->_tpl_vars['language'])."/lang_".($this->_tpl_vars['language']).".conf",'section' => 'index'), $this);?>

<?php echo smarty_function_config_load(array('file' => ($this->_tpl_vars['language'])."/dom.conf",'section' => 'main'), $this);?>

<?php if ($this->_tpl_vars['text']): ?>
  <section class="about-company">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <h3 class="title"><?php echo $this->_tpl_vars['title']; ?>
</h3>
              <hr>
              <p class="descr">
                <?php echo $this->_tpl_vars['text']; ?>

              </p>
            </div>
          </div>
        </div>
      </section>
      <?php endif; ?>
      <?php if ($this->_tpl_vars['perf']): ?>
      <section class="features">
        <div class="container">
          <div class="row">
            <h3 class="title"><?php echo $this->_config[0]['vars']['text3']; ?>
</h3>
            <div class="list-features">
              <div class="row">
              
                         <?php $i=0; ?>
           <?php $_from = $this->_tpl_vars['perf']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['aussen'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['aussen']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['module_data']):
        $this->_foreach['aussen']['iteration']++;
?>
                <div class="col-xs-6 col-sm-6"><i class="icon-check"></i><span class="klm" v="<?php echo $i; ?>"><?php echo $this->_tpl_vars['module_data']['slider_text1']; ?>
</span></span>
                </div>
                <?php $i++; if ($i % 2 == 0){ ?>
                   <div class="clearfix"></div>
                <?php } ?>
                <?php endforeach; endif; unset($_from); ?>
              </div>
            </div>
          </div>
        </div>
      </section><?php endif; ?>
      
      <?php if ($this->_tpl_vars['hit']): ?>        
<section class="hits">
        <div class="container">
          <h3 class="title"><?php echo $this->_config[0]['vars']['text1']; ?>
</h3>
          <div class="row">
           <?php $i=0; ?>
           <?php $_from = $this->_tpl_vars['hit']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['aussen'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['aussen']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['module_data']):
        $this->_foreach['aussen']['iteration']++;
?>
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="block-hit"><a href="<?php echo $this->_tpl_vars['module_data']['PRODUCTS_LINK']; ?>
"><img src="<?php echo $this->_tpl_vars['module_data']['PRODUCTS_IMAGE']; ?>
" class="hit-img resp"></a>
              <a href="<?php echo $this->_tpl_vars['module_data']['PRODUCTS_LINK']; ?>
" class="link-hit"><?php if ($this->_tpl_vars['module_data']['text'] == 14): ?><?php echo $this->_config[0]['vars']['type1']; ?>
<?php endif; ?><?php if ($this->_tpl_vars['module_data']['text'] == 13): ?><?php echo $this->_config[0]['vars']['type2']; ?>
<?php endif; ?><?php if ($this->_tpl_vars['module_data']['text'] == 15): ?><?php echo $this->_config[0]['vars']['type3']; ?>
<?php endif; ?>
 <span>«<?php echo $this->_tpl_vars['module_data']['PRODUCTS_NAME']; ?>
»</span><?php if ($this->_tpl_vars['module_data']['sh']): ?>, <?php echo $this->_tpl_vars['module_data']['sh']; ?>
 м²<?php endif; ?></a>
                <hr>
                <p class="price"><?php echo $this->_tpl_vars['module_data']['PRODUCTS_PRICE']; ?>
</p>
                <p class="old-price"><?php echo $this->_tpl_vars['module_data']['PRODUCTS_PRICE0']; ?>
</p>
              </div>
            </div>
            <?php $i++; if ($i % 2 == 0) echo '<div class="clearfix"></div>'; ?>
            <?php endforeach; endif; unset($_from); ?>
          </div>
        </div>
      </section>
<?php endif; ?>


      <section class="development">
        <div class="container">
          <div class="row">
            <div class="block-development">
              <p class="descr">
                <?php echo $this->_config[0]['vars']['con10']; ?>

              </p>
              <button type="button" class="link-dev"><?php echo $this->_config[0]['vars']['text4']; ?>
</button>
            </div>
          </div>
        </div>     
      </section>
  
      <?php if ($this->_tpl_vars['sotr']): ?>
      <section class="cooperation">
        <div class="container">
          <div class="row">
            <h3 class="title"><?php echo $this->_config[0]['vars']['text2']; ?>
</h3>
            <div class="block-cooperation">
              <div class="row">
                <div class="col-sm-12 col-md-10 col-md-offset-1">
                  <?php $i=1; ?>
                  <?php $_from = $this->_tpl_vars['sotr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['aussen'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['aussen']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['module_data']):
        $this->_foreach['aussen']['iteration']++;
?>
                  <div class="col-xs-6 col-sm-4 col-md-4"><a href="/step.html#a<?php echo $i; ?>" class="coop coop-<?php echo $i; ?>">
                      <div class="number"><span><?php echo $i; ?></span></div>
                      <div class="circle"><i class="icon <?php echo $this->_tpl_vars['module_data']['slider_text1']; ?>
"></i></div>
                      <p class="descr"><?php echo $this->_tpl_vars['module_data']['slider_title']; ?>
</p></a></div>
                      <?php $i++; ?>
                  <?php endforeach; endif; unset($_from); ?>    
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php endif; ?>
      <?php if ($this->_tpl_vars['ac_time']): ?>
      <section class="offer">
        <div class="container">
          <div class="row">
            <h3 class="title"><?php echo $this->_config[0]['vars']['con12']; ?>
</h3>
            <hr>
            <input type="hidden" id="ac_time" value="<?php echo $this->_tpl_vars['ac_time']; ?>
">
            <p class="descr">
             <?php echo $this->_tpl_vars['act_desc']; ?>

            </p><img src="<?php echo $this->_tpl_vars['ac_image']; ?>
" alt="Offer" class="offer-img resp">
            <p class="end-offer"><?php echo $this->_config[0]['vars']['con13']; ?>
</p>
            <div class="block-clock">
              <div class="line-days"></div>
              <div class="line-hours"></div>
              <div class="line-minutes"></div>
              <div class="clock"></div>
            </div>
          </div>
        </div>
      </section> <?php endif; ?>
      <section class="excursion">
        <div class="container">
          <div class="row">
            <h3 class="title" style="opacity: 0.6"><?php echo $this->_config[0]['vars']['head_mail_title']; ?>
</h3>
            <form class="subscribe" name="send2" id="send2">
              <div class="col-xs-4 col-sm-3 col-md-3">
                <input type="text" placeholder="<?php echo $this->_config[0]['vars']['head_mail_t5']; ?>
" name="name2">
              </div>
              <div class="col-xs-4 col-sm-3 col-md-3">
                <input type="text" placeholder="<?php echo $this->_config[0]['vars']['head_mail_t6']; ?>
" name="phone2">
              </div>
              <div class="col-xs-4 col-sm-3 col-md-3">
                <input type="email" placeholder="<?php echo $this->_config[0]['vars']['head_mail_t1']; ?>
" name="email2">
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3">
                <input type="submit" value="<?php echo $this->_config[0]['vars']['head_mail_t7']; ?>
" class="submit">
              </div>
            </form>
          </div>
        </div>
      </section>
      <?php echo $this->_tpl_vars['MODULE_reviews']; ?>

      <section class="discount">
        <div class="container">
          <div class="row">
            <!-- Если файл добавлен, добавить класс active--><i class="icon-status" style="display:none"></i>
            <i class="icon-status2" style="display:none"></i>
            <div class="col-xs-12 col-sm-6 col-md-6">
              <p class="descr"><?php echo $this->_config[0]['vars']['file_text']; ?>
</p>
            </div>
             <form id="comform" enctype="multipart/form-data" action="/page.php?page=mail::send3" target="rFrame" method="post" id="omsk">
             <input type="hidden" name="name13" value=""> 
             <input type="hidden" name="phone13" value=""> 
             <input type="hidden" name="time13" value=""> 
              <div class="col-xs-12 col-sm-3 col-md-3">
              
              <a class="upload-link a1"><i class="icon-clip2"></i><span class="upload-text"><?php echo $this->_config[0]['vars']['file_text1']; ?>
</span>
                  <input id="fa1" type="file" name="asc[]" class="upload-inp">
             </a>
             <a class="upload-link a2" style="display:none"><i class="icon-clip"></i><span class="upload-text"><?php echo $this->_config[0]['vars']['file_text2']; ?>
</span>
                  <input id="fa2" type="file" name="asc[]" class="upload-inp">
            </a>
            <a class="upload-link a3" style="display:none"><i class="icon-clip"></i><span class="upload-text"><?php echo $this->_config[0]['vars']['file_text2']; ?>
</span>
                  <input id="fa3" type="file" name="asc[]" class="upload-inp">
            </a>            
            <a class="upload-link a4" style="display:none"><i class="icon-clip3"></i><span class="upload-text"><?php echo $this->_config[0]['vars']['file_text3']; ?>
</span></a>
                     
                  </div>
              <div class="col-xs-12 col-sm-3 col-md-3">
                <!-- Если файл добавлен, добавить класс active-->
                <input type="submit" class="submit" value="<?php echo $this->_config[0]['vars']['file_button']; ?>
">
              </div>
            </form> 
            <iframe id="rFrame" name="rFrame" style="display: none"> </iframe> 
          </div>
        </div>
      </section>
	<?php echo $this->_tpl_vars['MODULE_latest_news']; ?>
  
      <section class="subscription">
        <div class="container">
          <div class="row">
            <form method="post" name="see_mail" id="mail_send">
              <div class="col-xs-12 col-sm-9 col-md-9">
                <div class="block-inputs">
                  <input type="email" name="email" placeholder="<?php echo $this->_config[0]['vars']['head_mail_t1']; ?>
" class="sub-inp">
                  <div class="checboxes">
                    <input id="checkbox1" type="checkbox" name="check1" class="checkbox">
                    <label for="checkbox1" class="check-label"></label>
                    <label for="checkbox1" class="label-name"><?php echo $this->_config[0]['vars']['head_mail_t2']; ?>
</label>
                    <input id="checkbox2" type="checkbox" name="check2" checked class="checkbox">
                    <label for="checkbox2" class="check-label"></label>
                    <label for="checkbox2" class="label-name"><?php echo $this->_config[0]['vars']['head_mail_t3']; ?>
</label>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3">
                <input type="submit" value="<?php echo $this->_config[0]['vars']['head_mail_t4']; ?>
" class="submit">
              </div>
            </form>
          </div>
        </div>
      </section>