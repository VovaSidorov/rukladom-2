<?php
    /*
    http://160.by/
    Евгений Фоминов
    */

    require('includes/application_top.php');

    if (isset($_GET['action']) && $_GET['action']=='process_true')
    {
        $messageStack->add('Плагин '.$_GET['modules'].' успешно выполнен', 'success');
    }

    if (isset($_GET['module']) && !empty($_GET['module'])) 
    {
        p::$module = $_GET['module'];
        p::$name = $_GET['module'];

        if (isset($_GET['action']))
        {
            switch ($_GET['action'])
            {
                case 'install': 
                    p::install();
                    p::redirect("plugins.php".'?module='.p::$name);      
                    break; 

                case 'remove': 
                    p::remove(); 
                    p::redirect("plugins.php".'?module='.p::$name);
                    break; 

                case 'process': p::process(); break;     
                case 'save': p::save_options(); break; 
                case 'multi_action': p::multi_action(); break;
            }
        }
    }

    if ( !defined('YES')) define('YES', 'Да');
    if ( !defined('NO')) define('NO', 'Нет');

    $_plug_array =  p::plug_array();
    
	
	
    if ( isset($_GET['page']))
    {
        if ( isset(p::$action['page_admin'][$_GET['page']]) )
        {
            $_plug_name = p::$action_plug[$_GET['page']];    
            p::$name = $_plug_name;
            p::set_dir();
            $_page = $_GET['page'];

            $_param = explode('::', $_page); 

            if ( count( $_param ) == 2 )
            {
                if ( class_exists ($_param[0]))
                {
                    if ( method_exists($_param[0], $_param[1]))
                    {
                        $result = call_user_func(array( $_param[0], $_param[1]));
                    }
                }
            }
            else
            {
                if ( function_exists($_page) )
                {
                    $_page(); 
                }
            }
            exit();
        }
        else
        {
            echo 'no page!';
            exit();
        }
    }

    $plugins = p::$info;




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>"> 
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <link rel="stylesheet" type="text/css" href="../images/plug/p.css">
        <script type="text/javascript" src="includes/general.js"></script>
        <?php if (p::$cms == 'osc') {?>
            <script language="javascript" src="includes/menu.js"></script>
            <script language="javascript" src="includes/general.js"></script>
            <link rel="stylesheet" type="text/css" href="../jscript/jquery/plugins/ui/css/smoothness/jquery-ui-1.8.7.custom.css">
            <script type="text/javascript" src="../jscript/jquery/jquery-1.4.2.min.js"></script>
            <script type="text/javascript" src="../jscript/jquery/plugins/ui/jquery-ui-1.8.6.min.js"></script>
            <?php } ?>
        <style>a.mn{font-size:11px;}</style>
    </head>

    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="SetFocus();">

        <!-- header //-->
        <?php require(DIR_WS_INCLUDES . 'header.php'); ?>
        <table border="0" width="100%" cellspacing="2" cellpadding="2">
            <tr>
                <?php if (p::$cms == 'osc') { ?>
                    <td width="<?php echo BOX_WIDTH; ?>" valign="top">

                        <table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
                            <!-- left_navigation //-->
                            <?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
                            <!-- left_navigation_eof //-->
                        </table></td>
                    <!-- body_text //-->
                    <?php 
                    }
                    if (p::$cms == 'osc') {?>
                    <script type="text/javascript">
                        $('#adminAppMenu').accordion({
                            autoHeight: false,
                            icons: {
                                'header': 'ui-icon-plus',
                                'headerSelected': 'ui-icon-minus'
                            }

                            ,active: 7
                        });
                    </script>
                    <?php } ?>
                <td class="boxCenter" width="100%" valign="top"> 
                    <?Php

                    ?>
                    <table border="0" width="100%" cellspacing="4" cellpadding="2" valign="top">
                        <tr>
                            <td  valign="top">
                                <?php
                                    if (isset($_GET['type']) and $_GET['type'] == 1 and isset($_GET['module']) and !empty($_GET['module']))
                                    {
                                        p::$name = $_GET['module'];

                                        if ( isset(p::$info[ p::$name ]['title'])) echo '<h1 class="contentBoxHeading">'.p::$info[ p::$name ]['title'].'</h1>';

                                        p::set_dir();
                                        p::option2();
                                    }
                                    elseif (isset($_GET['main_page']))
                                    {		
                                        p::$name=p::$action_plug[$_GET['main_page']];
										p::$module = p::$name;
										p::lang( p::$name );

										$_title = '';
										if ( isset( p::$lang[p::$name][ $_GET['main_page'] ]) )
										{
										   echo '<h1 class="contentBoxHeading">'.p::$lang[p::$name][ $_GET['main_page']].'</h1>';
										}
										elseif ( isset(p::$info[ p::$name ]['title'])) 
										{
										   echo '<h1 class="contentBoxHeading">'.p::$info[ p::$name ]['title'].'</h1>';
										} 

									
                                        if (isset(p::$action['main_page_admin'][$_GET['main_page']]))
                                        {
                                            $_param = explode('::', $_GET['main_page']);

                                            if ( count( $_param ) == 2 )
                                            {
                                                if ( class_exists ($_param[0]))
                                                {
                                                    if ( method_exists($_param[0], $_param[1]))
                                                    {
                                                        p::$name = p::$action_plug[$_GET['main_page']];   
                                                        p::set_dir();
                                                        $result = call_user_func(array( $_param[0], $_param[1]));
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (function_exists($_GET['main_page']))
                                                {
                                                    p::$name = p::$action_plug[$_GET['main_page']];   
                                                    p::set_dir();
                                                    $_GET['main_page'](); 
                                                }
                                            }

                                        }
                                        else
                                        {
                                            echo 'no page!';
                                        }
                                    }else
									 if ( isset($_GET['check']))
									{
									  
    echo '<h1 class="contentBoxHeading">Проверка файлов плагинной системы</h1>';
	   $ch = array();
	   $ch[] = array('path'=>'includes/application_top.php', 'num'=>array(2, 30));
	   $ch[] = array('path'=>'admin/includes/application_top.php', 'num'=>array(1));
	   $ch[] = array('path'=>'includes/header.php', 'num'=>array(3,4,5,6));
	   $ch[] = array('path'=>'page.php', 'num'=>array(7));
	   $ch[] = array('path'=>'admin/includes/header.php', 'num'=>array(8));
	   $ch[] = array('path'=>'includes/modules/product_info.php', 'num'=>array(9, 10));
	   $ch[] = array('path'=>'includes/modules/product_listing.php', 'num'=>array(11, 12, 13));
	   $ch[] = array('path'=>'includes/classes/product.php', 'num'=>array(14));
	   $ch[] = array('path'=>'includes/application_bottom.php', 'num'=>array(15));
	   $ch[] = array('path'=>'includes/center_modules.php', 'num'=>array(16));
	   $ch[] = array('path'=>'admin/includes/modules/new_product.php', 'num'=>array(17, 18));
	   $ch[] = array('path'=>'admin/includes/classes/categories.php', 'num'=>array(19,20,21,22,23 ));
	   $ch[] = array('path'=>'admin/includes/modules/categories_view.php', 'num'=>array(24,25 ));
	   $ch[] = array('path'=>'admin/categories.php', 'num'=>array(26) );
	   $ch[] = array('path'=>'admin/latest_news.php', 'num'=>array(27, 28, 29) );
	   $ch[] = array('path'=>'includes/classes/vam_price.php', 'num'=>array(31, 32) );
	   $ch[] = array('path'=>'includes/modules/default.php', 'num'=>array(33,35) );
	   $ch[] = array('path'=>'includes/modules/product_listing.php', 'num'=>array(34) );
	   $ch[] = array('path'=>'includes/header.php', 'num'=>array(36,37) );
	   $ch[] = array('path'=>'admin/includes/classes/categories.php', 'num'=>array(38 ));
	   
	   echo '<table>';
	   echo '<tr align="center">';
	   echo '<td width="50px">№</td>';
	   echo '<td>Файл</td>';
	   echo '<td width="100px">Статус</td>';
	   echo '</tr>';
	   $m=0;
	   foreach ($ch as $val)
	   {
	       $m++;
	       $file = DIR_FS_DOCUMENT_ROOT.$val['path'];
	       if ( is_file($file) )
		   {
		      $ff = file_get_contents($file);
			  echo '<tr align="center">';
			  echo '<td>'.$m.'. </td>';
			  echo '<td align="left">'.$val['path'].'</td>';
			  
			  $ok = false;
			  foreach ($val['num'] as $num => $v)
		      {
		          if ( strpos($ff, '/*plugins | 160.by @'.$v.'*/')===false )
				  {
				     $ok = true;
				  }
		      }
			  
			  if ( $ok == false)
			  {
			     echo '<td align="center"><font color="green">ok</font></td>';
			  }
			  else
			  {
			     echo '<td align="center"><font color="red">Ошибка</font></td>';
			  }
			  echo '</tr>';
		   }
	   }
	   echo '</table>';
	   exit();
	
									}
                                    else
                                    {
                                    ?>
                                    <table width="100%"><tr><td align="left"><h1 class="contentBoxHeading">Плагины:</h1></td><td align="right"><a class="mn" href="http://160.by/plugins/" target="_blank">Дополнительные плагины</a></td></tr></table>

                                    <table border="0" width="100%" cellspacing="0" cellpadding="0"  valign="top">
                                    <tr>
                                        <td valign="top">
                                        <table border="0" width="100%" cellspacing="2" cellpadding="2">
                                        <tr class="dataTableHeadingRow">
                                            <td class="dat1" width="20px">&nbsp;</td>
                                            <td class="dat1" width="300px">Плагины</td>
                                            <td class="dat1">Название плагина</td>
                                            <td class="dat1" width="50px">Версия</td>
                                            <td class="dat1" width="50px">Статус</td>
                                            <td class="dat1" align="right" width="50">Действие&nbsp;</td>
                                        </tr>		  
                                        <?php



                                            foreach ($_plug_array as $_plug_value => $_value)
                                            { 


                                                $color = $_value['color'];
                                                p::$name = $_value['name'];

                                                if (empty(p::$module)) p::$module = p::$name;

                                                if (p::$module == p::$name)
                                                {
                                                    echo '<tr class="dataTableRowSelected"'.$_through .'>' . "\n";
                                                }
                                                else
                                                { 
                                                    echo '<tr onmouseover="this.style.background=\'#e9fff1\';" onmouseout="this.style.background=\''.$color.'\';"  style="background-color:'.$color;
                                                    echo ';" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'"'.$_through .'>' . "\n";
                                                }  

                                            ?>				
                                            <td <?php echo 'onclick="document.location.href=\'' . "plugins.php?".'module='.p::$name.'\'"';  ?> align="center" valign="center"><?php  echo $_value['icon'];?></td>
                                            <td <?php echo 'onclick="document.location.href=\'' . "plugins.php?".'module='.p::$name.'\'"'; ?> class="dataTableContent"><?php  echo $_value['title'];?></td>
                                            <td <?php echo 'onclick="document.location.href=\'' . "plugins.php?".'module='.p::$name.'\'"'; ?> class="dataTableContent"><?php echo $_value['name']; ?></td>
                                            <td <?php echo 'onclick="document.location.href=\'' . "plugins.php?".'module='.p::$name.'\'"'; ?> class="dataTableContent" align="center"><?php echo (!empty($_value['version'])? $_value['version'] : '1.0'); ?></td>
                                            <td class="dataTableContent" align="center"><?php p::status();?></td>
                                            <td width="80px" class="dataTableContent" align="right" valign="center"><?php p::action(p::$name); ?></td> 
                                        </tr><?php

                                        }

                                    ?>
                                    <?php

                                        //блоки из текущего шаблона
                                        echo '</table><br /><div style="width:100%;text-align:right;font-size:12px;"><i>/includes/modules/plugins/</i>&nbsp;&nbsp;</div>';

                                    ?>
                                </td>
                                <td class="right_box" valign="top">
                                    <table class="contentTable4" border="0" width="237px" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="dat1">
                                                <?php 
                                                    if (!empty(p::$info[p::$module]['title']))
                                                    {
                                                        $_title = p::$info[p::$module]['title'];
                                                    }
                                                    else
                                                    {
                                                        if (isset(p::$info[p::$module]))
                                                        {
                                                            $_title =  ucfirst(p::$module);
                                                        }
                                                        else
                                                        {
                                                            $_title = '-:-:-';
                                                        }

                                                    }

                                                    if (mb_strlen($_title) > 27)  
                                                    {
                                                        $_title = _mb_substr($_title, 0, 27);
                                                        $_title = $_title.'..';
                                                    }
                                                    echo $_title;
                                            ?></td>
                                        </tr>
                                    </table>
                                    <?php p::option($_plug_array); ?> 
									<span style="margin:15px;"><a class="mn" href="plugins.php?check=1">Проверка плагинной системы</a></span><br>
									<span style="margin:15px;font-size:12px;"><i>Версия: <?php echo p::$version; ?></i></span>
                                </td>
                            </tr>
                        </table>
                        <?php
                        }
                    ?>
                </td>
            </tr>
        </table></td>
        </tr>
        </table></td>
        </tr>
        </table>

        </div>

        <!-- footer //-->
        <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
        <!-- footer_eof //-->
        <br />
    </body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>