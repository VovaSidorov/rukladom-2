<?php
/* --------------------------------------------------------------
/* --------------------------------------------------------------*/
require('includes/application_top.php');
require_once(DIR_FS_INC . 'vam_wysiwyg_tiny.inc.php');
require_once (DIR_FS_INC.'vam_image_submit.inc.php');


include(DIR_WS_CLASSES . 'language.php');
$lng = new language;


$languages_string = '';
$count_lng='';



reset($lng->catalog_languages);  



if ($_GET['action']) {
    switch ($_GET['action']) 
    {
        case 'delete_items': 
            if ($_POST['reviews_id']) {
                $reviews_id = vam_db_prepare_input($_POST['reviews_id']);
                vam_db_query("delete from reviews_description_main where reviews_id = '" . vam_db_input($reviews_id) . "'");
            }
            vam_redirect(vam_href_link('reviews2.php'));
            break;

        case 'insert_item': //insert a new news article.
            if ($_POST['reviews_text']) 
            {

                $up_error = false;
                if (!$banners_image = &vam_try_upload('image', DIR_FS_CATALOG_IMAGES.'reviews_main/' ) ) {
                    $up_error = true;
                }

                if ( $up_error == false)
                {
                    $sql_data_array = array('customers_name'   => vam_db_prepare_input($_POST['customers_name']),
                        'reviews_text'    => vam_db_prepare_input($_POST['reviews_text']),
                        'date_added' => 'now()',
                        'image'=> $_FILES['image']['name']
                    );
                }
                else
                {
                    $sql_data_array = array('customers_name'   => vam_db_prepare_input($_POST['customers_name']),
                        'reviews_text'    => vam_db_prepare_input($_POST['reviews_text']),
                        'date_added' => 'now()' );
                }

                $sql_data_array['language_id'] = (int)$_POST['language_id'];		  

                vam_db_perform('reviews_description_main', $sql_data_array);
                $reviews_id = vam_db_insert_id();

            }
            //       vam_redirect(vam_href_link('reviews2.php'));
            break;

        case 'update_item': //user wants to modify a news article.

            if($_GET['reviews_id']) 
            {

                $up_error = false;
                if (!$banners_image = &vam_try_upload('image', DIR_FS_CATALOG_IMAGES.'reviews_main/' ) ) {
                    $up_error = true;
                }

                if ( $up_error == false)
                {
                    $sql_data_array = array('customers_name'   => vam_db_prepare_input($_POST['customers_name']),
                        'reviews_text'    => vam_db_prepare_input($_POST['reviews_text']),
                        'date_added' => vam_db_prepare_input($_POST['date_added']),
                        'image'=> $_FILES['image']['name']
                    );
                }
                else
                {
                    $sql_data_array = array('customers_name'   => vam_db_prepare_input($_POST['customers_name']),
                        'reviews_text'    => vam_db_prepare_input($_POST['reviews_text']),
                        'date_added' => vam_db_prepare_input($_POST['date_added']),
                    );
                }		

                $sql_data_array['language_id'] = (int)$_POST['language_id'];
                vam_db_perform('reviews_description_main', $sql_data_array, 'update', "reviews_id = '" . vam_db_prepare_input($_GET['reviews_id']) . "'");




            }
            //      vam_redirect(vam_href_link('reviews2.php'));
            break;
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>"> 
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
        <?php 
        if ($_GET['action']=='new_items') echo vam_wysiwyg_tiny('latest_news',$data['code']); 
        ?>
    </head>
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
        <!-- header //-->
        <?php require(DIR_WS_INCLUDES . 'header.php'); ?>
        <!-- header_eof //-->

        <!-- body //-->
        <table border="0" width="100%" cellspacing="2" cellpadding="2">
            <tr>
                <?php if (ADMIN_DROP_DOWN_NAVIGATION == 'false') { ?>
                    <td width="<?php echo BOX_WIDTH; ?>" align="left" valign="top">
                        <!-- left_navigation //-->
                        <?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
                        <!-- left_navigation_eof //-->
                    </td>
                    <?php } ?>
                <!-- body_text //-->
                <td class="boxCenter" valign="top">

                    <?php 
                    $manual_link = 'add-news';
                    if ($_GET['action'] == 'new_items' and isset($_GET['reviews_id'])) {
                        $manual_link = 'edit-news';
                    }  
                    if ($_GET['action'] == 'delete_items_k') {
                        $manual_link = 'delete-news';
                    }  
                    ?>
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="pageHeading">Отзывы о магазине</td>
                            <td class="pageHeading" align="right">
                                <?php if ($_GET['action'] != 'new_items') { echo '&nbsp;<a class="button" href="' . vam_href_link('reviews2.php', 'action=new_items') . '"><span>' . vam_image(DIR_WS_IMAGES . 'icons/buttons/add.png', '', '12', '12') . '&nbsp;' . BUTTON_INSERT . '</span></a>'; } ?>&nbsp;<a class="button" href="<?php echo MANUAL_LINK_NEWS.'#'.$manual_link; ?>" target="_blank"><span><?php echo vam_image(DIR_WS_IMAGES . 'icons/buttons/information.png', '', '12', '12'); ?>&nbsp;<?php echo TEXT_MANUAL_LINK; ?></span></a></td>
                        </tr>
                    </table>

                    <table border="0" width="100%" cellspacing="0" cellpadding="2">
                        <?php
                        if ($_GET['action'] == 'new_items') 
                        { //insert or edit a news item
                            if ( isset($_GET['reviews_id']) ) { //editing exsiting news item
                                $__item_query = vam_db_query("select * from reviews_description_main where reviews_id = '" . $_GET['reviews_id'] . "'");
                                $__item = vam_db_fetch_array($__item_query);

                            } else { //adding new news item
                                $__item = array();
                            }

                            $st = '<select name="language_id">';
                            while (list($key, $value) = each($lng->catalog_languages)) 
                            {
                                if ($__item['language_id'] == $value['id'])
                                {
                                    $st .= '<option selected value="'.$value['id'].'">'.$key.'</option>';
                                }
                                else
                                {
                                    $st .= '<option value="'.$value['id'].'">'.$key.'</option>';   
                                }
                            }

                            $st .='</select>';
                            ?>
                            <tr><?php echo vam_draw_form('new_items', 'reviews2.php', isset($_GET['reviews_id']) ? vam_get_all_get_params(array('action')) . 'action=update_item' : vam_get_all_get_params(array('action')) . 'action=insert_item', 'post', 'enctype="multipart/form-data"'); ?>
                                <td><table border="0" cellspacing="0" cellpadding="2" width="100%">
                                        <tr>
                                            <td class="main" valign="top">ФИО:</td>
                                            <td class="main"><?php echo  vam_draw_input_field('customers_name', $__item['customers_name'], 'size="42"'); ?></td>
                                        </tr>
                                        <?php
                                        if ( isset($_GET['reviews_id']) ) {
                                            ?>		  
                                            <tr>
                                                <td class="main" valign="top">Дата:</td>
                                                <td class="main"><?php echo vam_draw_input_field('date_added', $__item['date_added'], 'size="42"'); ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>		  
                                        <tr>
                                            <td class="main" valign="top">Отзыв:</td>
                                            <td class="main"><?php echo  vam_draw_textarea_field('reviews_text', '', '40', '10', stripslashes($__item['reviews_text'])); ?><br /><a href="javascript:toggleHTMLEditor('reviews_text');"><?php echo TEXT_TOGGLE_EDITOR; ?></a></td>
                                        </tr>                                            
                                        <tr>
                                            <td class="main" valign="top">Язык:</td>
                                            <td class="main"><?php echo $st; ?></td>
                                        </tr>          
                                        <tr>
                                            <td class="main" valign="top"></td>
                                            <td class="main">&nbsp;</td>
                                        </tr>	
                                        <tr>
                                            <td class="main" valign="top">Картинка:</td>
                                            <td class="main">
                                                <?php echo vam_draw_file_field('image') . ' '  . DIR_FS_CATALOG_IMAGES.'reviews_main/'; ?><br><br>

                                                <?php 
                                                if ( is_file(DIR_FS_CATALOG_IMAGES.'reviews_main/'.$__item['image']) )
                                                {
                                                    echo '<img width="150px" src="'.HTTP_CATALOG_SERVER.'/'.DIR_WS_IMAGES.'reviews_main/'.$__item['image'].'" />';
                                                }
                                                ?>
                                            </td>
                                        </tr>

                                    </table></td>
                            </tr>
                            <tr>
                                <td class="main" align="right">
                                    <?php
                                    isset($_GET['reviews_id']) ? $cancel_button = '&nbsp;&nbsp;<a class="button" href="' . vam_href_link('reviews2.php', 'reviews_id=' . $_GET['reviews_id']) . '"><span>' . vam_image(DIR_WS_IMAGES . 'icons/buttons/cancel.png', '', '12', '12') . '&nbsp;' . BUTTON_CANCEL . '</span></a>' : $cancel_button = '';
                                    echo '<span class="button"><button type="submit" value="' . BUTTON_INSERT .'">' . vam_image(DIR_WS_IMAGES . 'icons/buttons/submit.png', '', '12', '12') . '&nbsp;' .BUTTON_INSERT . '</button></span>' . $cancel_button;
                                    ?>
                                </td>
                                </form></tr>
                            <?php

                        } else {
                            ?>
                            <tr>
                                <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td valign="top"><table border="0" width="100%" cellspacing="2" cellpadding="0" class="contentListingTable">
                                            <tr class="dataTableHeadingRow">
                                                <td class="dataTableHeadingContent">ФИО</td>
                                                <td class="dataTableHeadingContent" align="center">Отзыв</td>
                                                <td class="dataTableHeadingContent" align="center">Дата</td>
                                                <td class="dataTableHeadingContent" align="center">Язык</td>
                                                <td class="dataTableHeadingContent" align="right">&nbsp;</td>
                                            </tr>
                                            <?php
                                            $rows = 0;

                                            $__item_count = 0;
                                            $__item_query_raw = 'select reviews_id, customers_name, date_added, reviews_text, image, language_id from reviews_description_main order by date_added desc';

                                            $__item_split = new splitPageResults($_GET['page'], MAX_DISPLAY_ADMIN_PAGE, $__item_query_raw, $__item_query_numrows);

                                            $__item_query = vam_db_query($__item_query_raw);
     
                                            $am = array();
                                            foreach ($lng->catalog_languages as $_n => $_v)
                                            {
                                                $am[ $_v['id'] ] = $_n; 
                                            }
                                            while ($__item = vam_db_fetch_array($__item_query)) 
                                            {
                                                $__item_count++;
                                                $rows++;

                                                if (((!$_GET['reviews_id']) || (@ $_GET['reviews_id'] == $__item['reviews_id'])) && (!$nInfo)) {
                                                    $nInfo = new objectInfo($__item);
                                                }

                                                if ((is_object($nInfo)) && ($__item['reviews_id'] == $nInfo->reviews_id)) {

                                                    echo '<tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'" onclick="document.location.href=\'' . vam_href_link('reviews2.php', vam_get_all_get_params(array('reviews_id','action')) . 'reviews_id=' . $__item['reviews_id']) . '\'">' . "\n";
                                                } else {
                                                    echo '<tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . vam_href_link('reviews2.php', vam_get_all_get_params(array('reviews_id','action')) . 'reviews_id=' . $__item['reviews_id']) . '\'">' . "\n";
                                                }
                                                ?>
                                                <td class="dataTableContent"><?php echo '&nbsp;' . $__item['customers_name']; ?></td>
                                                <td class="dataTableContent" align="center"><?php echo $__item['reviews_text']; ?></td>
                                                <td class="dataTableContent" align="center"><?php echo $__item['date_added']; ?></td>
                                                <td class="dataTableContent" align="center"><?php echo $am[$__item['language_id']]; ?></td>
                                                <td class="dataTableContent" align="right"><?php 
                                                    if ($__item['reviews_id'] == $_GET['reviews_id']) 
                                                    { 
                                                        echo vam_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); 
                                                    } else { echo '<a href="' . vam_href_link('reviews2.php', vam_get_all_get_params(array('reviews_id','action', 'flag')) . 'reviews_id=' . $__item['reviews_id']) . '">' . vam_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
                                            </tr>
                                            <?php
                                        }

                                        ?>
                                        <tr>
                                            <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                                                    <tr>
                                                        <td align="right" class="smallText"><?php echo '&nbsp;<a class="button" href="' . vam_href_link('reviews2.php', 'action=new_items') . '"><span>' . vam_image(DIR_WS_IMAGES . 'icons/buttons/add.png', '', '12', '12') . '&nbsp;' . BUTTON_INSERT . '</span></a>'; ?>&nbsp;</td>
                                                    </tr>																																		  
                                                </table></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                                                    <tr>
                                                        <td class="smallText" valign="top"><?php echo $__item_split->display_count($__item_query_numrows, MAX_DISPLAY_ADMIN_PAGE, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_NEWS); ?></td>
                                                        <td class="smallText" align="right"><?php echo $__item_split->display_links($__item_query_numrows, MAX_DISPLAY_ADMIN_PAGE, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], vam_get_all_get_params(array('page', 'action', 'x', 'y', 'reviews_id'))); ?></td>
                                                    </tr>              
                                                </table></td>
                                        </tr>
                                    </table></td>
                                <?php
                                $heading = array();
                                $contents = array();
                                switch ($_GET['action']) {
                                    case 'delete_items_k': //generate box for confirming a news article deletion
                                        $heading[] = array('text'   => '<b>Удалить отзыв</b>');

                                        $contents = array('form'    => vam_draw_form('news', 'reviews2.php', vam_get_all_get_params(array('action')) . 'action=delete_items') . vam_draw_hidden_field('reviews_id', $_GET['reviews_id']));
                                        $contents[] = array('text'  => 'Вы действительно хотите удалить этот отзыв?');
                                        $contents[] = array('text'  => '<br><b>' . $selected_item['headline'] . '</b>');

                                        $contents[] = array('align' => 'center',
                                            'text'  => '<br><span class="button"><button type="submit" value="' . BUTTON_DELETE .'">' . vam_image(DIR_WS_IMAGES . 'icons/buttons/delete.png', '', '12', '12') . '&nbsp;' . BUTTON_DELETE . '</button></span><a class="button" href="' . vam_href_link('reviews2.php',  vam_get_all_get_params(array ('reviews_id', 'action')).'reviews_id=' . $selected_item['reviews_id']) . '"><span>' . vam_image(DIR_WS_IMAGES . 'icons/buttons/cancel.png', '', '12', '12') . '&nbsp;' . BUTTON_CANCEL . '</span></a>');
                                        break;

                                    default:
                                        if ($rows > 0) {
                                            if (is_object($nInfo)) { //an item is selected, so make the side box
                                                $heading[] = array('text' => '<b>Информация</b>');

                                                $contents[] = array('align' => 'center', 
                                                    'text' => '<a class="button" href="' . vam_href_link('reviews2.php',  vam_get_all_get_params(array ('reviews_id', 'action')).'reviews_id=' . $nInfo->reviews_id . '&action=new_items') . '"><span>' . vam_image(DIR_WS_IMAGES . 'icons/buttons/edit.png', '', '12', '12') . '&nbsp;' . BUTTON_EDIT . '</span></a> <a class="button" href="' . vam_href_link('reviews2.php',  vam_get_all_get_params(array ('reviews_id', 'action')).'reviews_id=' . $nInfo->reviews_id . '&action=delete_items_k') . '"><span>' . vam_image(DIR_WS_IMAGES . 'icons/buttons/delete.png', '', '12', '12') . '&nbsp;' . BUTTON_DELETE . '</span></a>');

                                                $contents[] = array('text' => '<br>' . $nInfo->content);
                                            }
                                        } else { // create category/product info
                                            $heading[] = array('text' => '<b>' . EMPTY_CATEGORY . '</b>');

                                            $contents[] = array('text' => sprintf(TEXT_NO_CHILD_CATEGORIES_OR_PRODUCTS, $parent_categories_name));
                                        }
                                        break;
                                }

                                if ( (vam_not_null($heading)) && (vam_not_null($contents)) ) {
                                    echo '            <td width="25%" valign="top">' . "\n";

                                    $box = new box;
                                    echo $box->infoBox($heading, $contents);

                                    echo '            </td>' . "\n";
                                }
                                ?>
                            </tr>
                        </table></td>
                </tr>
                <?php
            }
            ?>
        </table></td>
        <!-- body_text_eof //-->
        </tr>
        </table>
        <!-- body_eof //-->

        <!-- footer //-->
        <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
        <!-- footer_eof //-->
        <br>
    </body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>